<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\StudentAttendance
 *
 * @property integer $id
 * @property string $date
 * @property integer $studentId
 * @property string $status
 * @method static \Illuminate\Database\Query\Builder|\App\Models\StudentAttendance whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\StudentAttendance whereDate($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\StudentAttendance whereStudentId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\StudentAttendance whereStatus($value)
 * @mixin \Eloquent
 */
class StudentAttendance extends Model
{
    protected $table = 'student_attendance';
}

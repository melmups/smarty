<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\StaffAttendance
 *
 * @property integer $id
 * @property string $clockInTime
 * @property integer $staffId
 * @method static \Illuminate\Database\Query\Builder|\App\Models\StaffAttendance whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\StaffAttendance whereClockInTime($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\StaffAttendance whereStaffId($value)
 * @mixin \Eloquent
 * @property string $date
 * @method static \Illuminate\Database\Query\Builder|\App\Models\StaffAttendance whereDate($value)
 * @property string $clockOutTime
 * @method static \Illuminate\Database\Query\Builder|\App\Models\StaffAttendance whereClockOutTime($value)
 */
class StaffAttendance extends Model
{
    protected $table = 'staff_attendance';

    public $timestamps = [];

    public function user()
    {
        return $this->belongsTo('App\Models\Staff', "id", "staffId");
    }
}

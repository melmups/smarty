<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\Grade
 *
 * @property integer $id
 * @property integer $level
 * @property string $label
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Grade whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Grade whereLevel($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Grade whereLabel($value)
 * @mixin \Eloquent
 */
class Grade extends Model
{
    public $timestamps = [];
    protected $table = "grade_levels";
    protected $fillable = ["level", "label"];
}

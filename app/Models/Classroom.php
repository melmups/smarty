<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\Classroom
 *
 * @property integer $id
 * @property string $identifier
 * @property integer $gradeId
 * @property string $classTeachers
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Classroom whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Classroom whereIdentifier($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Classroom whereGradeId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Classroom whereClassTeachers($value)
 * @mixin \Eloquent
 */
class Classroom extends Model
{
    public $timestamps = [];
    protected $table = "classes";
    protected $fillable = ["identifier", "gradeId"];
}

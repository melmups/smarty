<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\StaffQualification
 *
 * @property integer $id
 * @property string $name
 * @property string $institution
 * @property string $type
 * @property string $dateCompleted
 * @property integer $staffId
 * @method static \Illuminate\Database\Query\Builder|\App\Models\StaffQualification whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\StaffQualification whereName($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\StaffQualification whereInstitution($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\StaffQualification whereType($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\StaffQualification whereDateCompleted($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\StaffQualification whereStaffId($value)
 * @mixin \Eloquent
 */
class StaffQualification extends Model
{
    protected $table = 'staff_qualifications';
}

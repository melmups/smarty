<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\Fees
 *
 * @property integer $id
 * @property string $recieptId
 * @property string $paymentDate
 * @property float $amount
 * @property integer $studentId
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Fees whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Fees whereRecieptId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Fees wherePaymentDate($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Fees whereAmount($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Fees whereStudentId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Fees whereCreatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Fees whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class Fees extends Model
{
    protected $table = "fees_payments";

    protected $fillable = ['recieptId', 'paymentDate', 'amount', 'studentId'];
}

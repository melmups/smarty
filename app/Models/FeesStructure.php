<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\FeesStructure
 *
 * @property integer $id
 * @property integer $gradeId
 * @property float $amount
 * @method static \Illuminate\Database\Query\Builder|\App\Models\FeesStructure whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\FeesStructure whereGradeId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\FeesStructure whereAmount($value)
 * @mixin \Eloquent
 */
class FeesStructure extends Model
{
    protected $table = "fees_structure";
}

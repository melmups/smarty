<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\Student
 *
 * @property integer $id
 * @property string $firstNames
 * @property string $lastName
 * @property string $gender
 * @property string $dateOfBirth
 * @property string $birthEntryNo
 * @property string $residentialArea
 * @property string $guardianName
 * @property string $guardianPhone
 * @property string $medicalConditions
 * @property integer $classId
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Student whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Student whereFirstNames($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Student whereLastName($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Student whereGender($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Student whereDateOfBirth($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Student whereBirthEntryNo($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Student whereResidentialArea($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Student whereGuardianName($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Student whereGuardianPhone($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Student whereMedicalConditions($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Student whereClassId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Student whereCreatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Student whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class Student extends Model
{
    protected $table = "students";

    protected $fillable = [
        'firstNames',
        'lastName',
        'gender',
        'dateOfBirth',
        'birthEntryNo',
        'residentialArea',
        'guardianName',
        'guardianPhone',
        'medicalConditions',
        'classId'
    ];
}

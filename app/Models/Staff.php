<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\Staff
 *
 * @property integer $id
 * @property string $firstNames
 * @property string $lastName
 * @property string $gender
 * @property string $dateOfBirth
 * @property string $nationalId
 * @property string $ecNumber
 * @property string $nextOfKin
 * @property string $phone
 * @property string $homeAddress
 * @property integer $staffId
 * @property string $type
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Staff whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Staff whereFirstNames($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Staff whereLastName($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Staff whereGender($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Staff whereDateOfBirth($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Staff whereNationalId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Staff whereEcNumber($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Staff whereNextOfKin($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Staff wherePhone($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Staff whereHomeAddress($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Staff whereStaffId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Staff whereType($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Staff whereCreatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Staff whereUpdatedAt($value)
 * @mixin \Eloquent
 * @property-read \App\Models\User $user
 */
class Staff extends Model
{
    protected $table = 'staff';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'firstNames',
        'lastName',
        'gender',
        'dateOfBirth',
        'password',
        'nationalId',
        'ecNumber',
        'phone',
        'homeAddress',
        'type'
    ];

    public function user()
    {
        return $this->belongsTo('App\Models\User');
    }
}

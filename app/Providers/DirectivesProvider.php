<?php

namespace App\Providers;

use App\Models\Classroom;
use App\Models\Staff;
use App\Models\Student;
use Carbon\Carbon;
use Illuminate\Support\Facades\Blade;
use Illuminate\Support\ServiceProvider;

/**
 * Adds some blade directives when the application boots up
 */
class DirectivesProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        $this->addBladeDirectives();
    }

    public function addBladeDirectives()
    {
        Blade::directive('year', function () {
            $year = Carbon::now()->year;
            return "<?php echo {$year}; ?>";
        });

        Blade::directive('studentsCount', function () {
			$count = Student::all()->count();
            return "<?php echo {$count}?>";
        });

        Blade::directive('staffCount', function () {
            $count = Staff::all()->count();
            return "<?php echo {$count}; ?>";
        });

        Blade::directive('classCount', function () {
            $count = Classroom::all()->count();
            return "<?php echo {$count}; ?>";
        });


    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}

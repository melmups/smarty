<?php

Route::group(["middleware" => ["setup_complete"], "middlewareGroups" => "web"], function () {

    /* Handle application installation */
    Route::get('/install', "InstallController@showSetup");
    Route::post('/install', "InstallController@handleSetup");
});

Route::group(["middleware" => ["setup"], "middlewareGroups" => "web"], function () {

    Route::get('/', "PagesController@index");

    /* Handle staff clock ins */
    Route::get('/clock-in', "ClockInController@showRecord");
    Route::post('/clock-in', "ClockInController@handleRecord");



    /* User authentication routes */
    Route::get('/login', "AuthController@show");
    Route::post('/login', "AuthController@handle");
    Route::get('/logout', "AuthController@logout");
    /* End authentication routes */

    Route::get('/app', "AuthController@redirect");


    Route::group(['prefix' => '/app/admin', 'middleware' => ['auth', 'lock']], function () {

        Route::group(["middleware" => "setup.internal.reverse"], function () {

            Route::get("/setup", "AdminController@showSetup");
            Route::post("/setup", "AdminController@handleSetupData");

        });

        Route::group(["middleware" => "setup.internal"], function () {

            Route::get("/", "AdminController@index");

            Route::get("/settings", "AdminController@showSettings");

            Route::get("/staff/add", "AdminController@showAddStaff");
            Route::post('/staff/add', "AdminController@handleAddStaff");

            Route::get("/staff/view/{id}", "AdminController@showStaffMember");
            Route::post('/staff/update/{id}', "AdminController@updateStaff");

            Route::get("/staff/list", "AdminController@showListStaff");

            Route::get("/staff/clock-ins", "AdminController@showStaffClockIns");

            Route::get("/staff/clock-ins/day/{date}", "AdminController@handleFetchDayClockIns");
            Route::get("/staff/clock-ins/user/{id}", "AdminController@handleFetchUserClockIns");




            Route::get("/class/add", "AdminController@showAddClass");
            Route::post("/class/add", "AdminController@addClass");

            Route::get("/grade/add", "AdminController@showAddGrade");
            Route::post("/grade/add", "AdminController@handleAddGrade");

            Route::get("/class/list", "AdminController@showListClasses");


            Route::get('/students/add', "AdminController@showAddStudent");
            Route::post('/students/add', "AdminController@handleAddStudent");

            Route::get("/students/list", "AdminController@showListStudents");
            Route::get("/students/attendance", "AdminController@showAttendanceStatistics");

            Route::get("/fees/add", "AdminController@showAddFees");
            Route::post("/fees/add", "AdminController@handleAddFees");
            Route::post("/fees/add/find-student", "AdminController@handleFindStudent");
        });
    });
});

<?php

namespace App\Http\Middleware;

use Closure;
use Setting;

class SetupMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \Closure $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if ((bool)Setting::get('setup') === true) {
            return redirect('/app');
        }

        return $next($request);
    }
}

<?php
/**
 * Created by PhpStorm.
 * User: Melvin
 * Date: 11/17/2016
 * Time: 11:55 PM
 */

namespace App\Http\Middleware;

use Setting;
use Closure;

class InternalSetup
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \Closure $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        return $next($request);

        if((bool)Setting::get('internal_setup') !== true) {
            return redirect('/app/admin/setup')->with('messages', [
                [
                    'type' => 'warning',
                    'title' => "Setup application",
                    'message' => 'You need to finish setting up the application before you can use it'
                ]
            ]);
        }

        return $next($request);
    }
}
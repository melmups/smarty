<?php

namespace App\Http\Middleware;

use Closure;
use Setting;

class CheckStatus
{
    /**
     * Run the request filter.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \Closure $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if ((bool)Setting::get('setup') === false) {
            return redirect('/install');
        }

        return $next($request);
    }
}

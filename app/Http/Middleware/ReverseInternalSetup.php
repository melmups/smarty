<?php
/**
 * Created by PhpStorm.
 * User: Melvin
 * Date: 11/18/2016
 * Time: 12:12 AM
 */

namespace App\Http\Middleware;

use Setting;
use Closure;

class ReverseInternalSetup
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \Closure $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if((bool)Setting::get('internal_setup') === true) {
            return Setting::get("internal_setup");
        }

        return $next($request);
    }
}
<?php
/**
 * Created by PhpStorm.
 * User: Melvin
 * Date: 11/17/2016
 * Time: 10:39 PM
 */

namespace App\Http\Middleware;

use Auth;
use Closure;
use Session;

class LockMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \Closure $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (Auth::user()) {
            if (Session::has('app_lock')) {
                if ((bool)Session::get('app_lock') !== false) {
                    return redirect()->to('/app/unlock')->with('messages', [
                        [
                            'type' => 'warning',
                            'title' => 'Application locked',
                            'message' => 'Please enter your password to unlock'
                        ]
                    ]);
                }
            }
        }

        return $next($request);
    }
}
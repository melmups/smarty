<?php

namespace App\Http\Controllers;

use App\Models\Staff;
use App\Models\StaffAttendance;
use Illuminate\Http\Request;
use Smarty\Form\RecordClockInForm;
use Smarty\Form\ValidationException;

class ClockInController extends Controller
{

    /**
     * Show the clock-in page
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function showRecord()
    {
        return view('pages.clock-in');
    }

    public function handleRecord(Request $request, RecordClockInForm $rForm)
    {
        try {
            $rForm->validate($request->input());
        } catch (ValidationException $e) {
            return redirect()->back()->withInput()
                ->with('errors', $e->getErrors());
        }

        $ecNumber = strtolower($request->input('teacherEcNumber'));

        $staff = Staff::where("ecNumber", $ecNumber);

        if($staff->count() == 0) {

            return redirect()->back()->withInput()
                ->with('errors', [
                    [
                        "title" => "Staff member not found",
                        "message" => "No staff member matching that EC number was found. Please recheck it and try again",
                        "type" => "warning"
                    ]
                ]);

        } else {
            $staff = $staff->get()->first();
        }

        $a = app('mailer');

        $today = date('Y-m-d');

        if($request->has("check-in")) {

            $check = StaffAttendance::where([
                ['date', '=', $today],
                ['staffId', '=', $staff->id]
            ]);

            if($check->count() != 0) {
                return redirect()->back()->withInput()
                    ->with('errors', [
                        [
                            "title" => "Duplicate entry",
                            "message" => "Staff member was already clocked in",
                            "type" => "warning"
                        ]
                    ]);
            }

            $clockIn =  new StaffAttendance();

            $clockIn->date = $today;
            $clockIn->clockInTime = date('Y-m-d h:i:s');
            $clockIn->staffId = $staff->id;

            if($clockIn->save())
            {
                return redirect()->back()->withInput()
                    ->with('errors', [
                        [
                            "title" => "Clock in recorded",
                            "message" =>  $staff->firstNames . " " . $staff->lastName . " was clocked in successfully",
                            "type" => "warning"
                        ]
                    ]);
            }

        } else {

            $check = StaffAttendance::where([
                ['date', '=', $today],
                ['staffId', '=', $staff->id]
            ]);

            if($check->count() == 0) {
                return redirect()->back()->withInput()
                    ->with('errors', [
                        [
                            "title" => "Staff member not checked in",
                            "message" => "The staff member with the provided E.C number has not been checked in",
                            "type" => "warning"
                        ]
                    ]);
            }

            $check = StaffAttendance::where([
                ['clockOutTime', '=', "0000-00-00 00:00:00"],
                ['date', '=', $today],
                ['staffId', '=', $staff->id]
            ]);

            if($check->count() == 0) {
                return redirect()->back()->withInput()
                    ->with('errors', [
                        [
                            "title" => "Staff member already checked out",
                            "message" => "The staff member with the provided E.C number has already been checked out",
                            "type" => "warning"
                        ]
                    ]);
            }

            $time = $check->get()->first();

            $clockOut =  StaffAttendance::find($time->id);

            $clockOut->clockOutTime = date('Y-m-d h:i:s');

            if($clockOut->save())
            {
                return redirect()->back()->withInput()
                    ->with('errors', [
                        [
                            "title" => "Check out recorded",
                            "message" =>  $staff->firstNames . " " . $staff->lastName . " was checked out successfully",
                            "type" => "warning"
                        ]
                    ]);
            }

        }

    }
}

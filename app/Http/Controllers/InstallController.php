<?php

namespace App\Http\Controllers;

use Smarty\Form\SetupForm;
use Smarty\Form\ValidationException;
use Smarty\Helper;
use App\Models\User;
use Hash;
use Illuminate\Http\Request;
use Setting;
use Storage;

class InstallController extends Controller
{
    /**
     * Validator for setup form
     * @var \Smarty\Form\SetupForm
     */
    protected $setupForm;

    /**
     * InstallController constructor.
     * @param SetupForm $setupForm
     */
    public function __construct(SetupForm $setupForm)
    {
        $this->setupForm = $setupForm;
    }

    /**
     * Returns setup page to user
     */
    public function showSetup()
    {
        return view('app.setup.index');
    }

    /**
     * Handles the setup request
     * @param Request $request
     * @param Helper $helper
     * @return \Illuminate\Http\RedirectResponse
     */
    public function handleSetup(Request $request, Helper $helper)
    {
        try {
            $this->setupForm->validate($request->all());
        } catch (ValidationException $e) {
            return back()->withInput($request->input())->withErrors($e->getErrors());
        }

        $data = $request->input();

        $logo = $request->file('schoolLogo');

        if (!is_null($logo)) {
            $data['logoFilename'] = "upload_" . uniqid() . "." . $logo->getClientOriginalExtension();

            Storage::put($data['logoFilename'], file_get_contents($logo));
        } else {
            return back()->withInput($request->input())->withErrors([
                [
                    "title" => "Upload failed",
                    "message" => "An error occurred while uploading the school logo. Please try again",
                    "type" => "error"
                ]
            ]);
        }

        if (Storage::exists($data['logoFilename'])) {
            if (User::whereEmail($data["adminEmail"])->count() == 0) {
                $user = new User();

                $user->name = $data['adminName'];
                $user->email = $data['adminEmail'];
                $user->password = Hash::make($data['adminPassword']);
                $user->save();
            } else {

                Storage::delete($data['logoFilename']);

                return back()->withInput($request->input())->withErrors([
                    [
                        "title" => "User exists",
                        "message" => "A user with those login credentials was found in the application database. Please clear the application database and try again",
                        "type" => "warning"
                    ]
                ]);
            }
        } else {
            return back()->withInput($request->input())->withErrors([
                [
                    "title" => "Upload failed",
                    "message" => "An error occurred while uploading the school logo. Please try again",
                    "type" => "error"
                ]
            ]);
        }


        if (User::whereEmail($data["adminEmail"])->count() != 0) {
            $this->persistApplicationSettings($data);
        }


        $data['schoolType'] == "primary" ? $type = 0 : $type = 1;

        if($type > -1) {
            $this->insertBaseGrades($type, $helper);
        }


        return redirect('/login')->with(
            "messages", [
            [
                "type" => "success",
                "title" => "Success!",
                "message" => "School information was recorded successfully. Please login using the administrator credentials you entered on the previous page"
            ]
        ]);
    }


    /**
     * Handles inserting the base grades for the school type selected by the user
     * @param int $type
     * @param Helper $helper
     */
    public function insertBaseGrades(int $type, Helper $helper)
    {
        if ($type == 0) {
            $helper->insertPrimaryGrades();
        } else {
            $helper->insertSecondaryGrades();
        }
    }


    /**
     * Persist the provided application settings to the application's database
     * @param array $data
     */
    public function persistApplicationSettings(array $data)
    {
        Setting::set('school_name', $data['schoolName']);
        Setting::set('school_phone', $data['schoolPhone']);
        Setting::set('school_address', $data['schoolAddress']);
        Setting::set('school_logo', $data['logoFilename']);
        Setting::set('school_address', $data['schoolAddress']);
        Setting::set('school_type', $data['schoolType']);
        Setting::set('classes_added', 0);

        Setting::set('setup', 1);

        Setting::save();
    }


}

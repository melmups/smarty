<?php

namespace App\Http\Controllers;

use Smarty\Form\LoginForm;
use Smarty\Form\ValidationException;
use Auth;
use Illuminate\Http\Request;

class AuthController extends Controller
{
    protected $loginForm;

    public function __construct(LoginForm $loginForm)
    {
        $this->loginForm = $loginForm;
    }

    public function show()
    {
        if (Auth::check()) {
            switch (Auth::user()->type) {
                case 0:
                    return redirect()->to('/app/admin')->with("messages", [
                        [
                            "title" => "Already logged in!",
                            "message" => "You are already logged in",
                            "type" => "warning"
                        ]
                    ]);
                    break;

                case 1:
                    return redirect()->to('/app/teachers')->with("messages", [
                        [
                            "title" => "Already logged in!",
                            "message" => "You are already logged in",
                            "type" => "warning"
                        ]
                    ]);
                    break;

                case 2:
                    return redirect()->to('/app/teachers')->with("messages", [
                        [
                            "title" => "Already logged in!",
                            "message" => "You are already logged in",
                            "type" => "warning"
                        ]
                    ]);
                    break;
            }
        }

        if (Auth::viaRemember()) {
            $this->redirect();
        }

        return view('app.login');
    }

    public function redirect()
    {
        if (!Auth::check()) {
            return redirect()->to('/login')->with("messages", [
                [
                    "title" => "You need to sign in first",
                    "message" => "You need to sign in to view the requested page",
                    "type" => "warning"
                ]
            ]);
        }

        switch (Auth::user()->type) {
            case 0:
                return redirect()->to('/app/admin');
                break;

            case 1:
                return redirect()->to('/app/teachers');
                break;

            case 2:
                return redirect()->to('/app/secretary');
                break;
        }
    }

    public function logout()
    {
        if (!Auth::check()) {
            return redirect()->to('/');
        }

        Auth::logout();

        return redirect()->to('/login')->with("messages", [
            "title" => "Signed out",
            "message" => "You have been signed out successfully",
            "type" => "success"
        ]);
    }

    public function handle(Request $request)
    {
        try {
            $this->loginForm->validate($request->input());
        } catch (ValidationException $e) {
            return redirect()->back()->withInput()->with('error', $e->getErrors());
        }

        $email = $request->input('email');
        $password = $request->input('password');

        if (!Auth::attempt(['email' => $email, 'password' => $password])) {
            return redirect()->back()->withInput()->withErrors([
                [
                    "title" => "Authentication failed",
                    "message" => 'Invalid username or password',
                    "type" => "error"
                ]
            ]);
        }

        switch (Auth::user()->type) {
            case 0:
                return redirect()->to('/app/admin')->with('messages', [
                    [
                        "title" => "Hey there!",
                        "message" => "Welcome back, " . Auth::user()->name,
                        "type" => "success"
                    ]
                ]);
                break;

            case 1:
                return redirect()->to('/app/teachers');
                break;

            case 2:
                return redirect()->to('/app/secretary');
                break;
        }

        return true;
    }


}

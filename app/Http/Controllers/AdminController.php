<?php

namespace App\Http\Controllers;

use App\Models\StaffAttendance;
use Carbon\Carbon;
use Illuminate\Support\Collection;
use Smarty\Data\Staff\StaffModel;
use Smarty\Form\AddClassForm;
use Smarty\Form\AddGradeForm;
use Smarty\Form\AddStudentForm;
use Smarty\Form\AddTeacherForm;
use Smarty\Form\FeesPaymentForm;
use Smarty\Form\FindStudentForm;
use Smarty\Form\ValidationException;
use Smarty\Helper;
use App\Models\Classroom;
use App\Models\Fees;
use App\Models\Grade;
use App\Models\Staff;
use App\Models\Student;
use App\Models\User;
use Illuminate\Http\Request;
use Setting;
use Storage;
use Event;

class AdminController extends Controller
{
    /**
     * Validator for add teacher form
     * @var \Smarty\Form\AddTeacherForm
     */
    protected $addTeacherForm;

    /**
     * Validator for add grade form
     * @var AddGradeForm
     */
    protected $addGradeForm;
    /**
     * Validator for add student form
     * @var \Smarty\Form\AddStudentForm
     */
    protected $addStudentForm;

    /**
     * Validator for add class form
     * @var \Smarty\Form\AddClassForm
     */
    protected $addClassForm;

    /**
     * Validator for find student form
     * @var \Smarty\Form\FindStudentForm
     */
    protected $findStudentForm;

    /**
     * Validator for fees payment form
     * @var \Smarty\Form\FeesPaymentForm
     */
    protected $feesPaymentForm;


    public function __construct(
        AddTeacherForm $addTeacherForm,
        AddClassForm $addClassForm,
        AddGradeForm $addGradeForm,
        AddStudentForm $addStudentForm,
        FindStudentForm $findStudent,
        FeesPaymentForm $feesPayment
    ) {
        $this->addTeacherForm = $addTeacherForm;
        $this->addClassForm = $addClassForm;
        $this->addGradeForm = $addGradeForm;
        $this->addStudentForm = $addStudentForm;
        $this->findStudentForm = $findStudent;
        $this->feesPaymentForm = $feesPayment;
    }


    public function index(Helper $helper)
    {
        $logo = base64_encode(Storage::get(Setting::get('school_logo')));

        return view('app.dashboard.admin.index')->with([
            'boyToGirlRatio' => $helper->calculateMaleToFemaleRatio(),
            'logo' => $logo
        ]);
    }

    /**
     * Returns the assign staff page to the user
     * @return mixed
     */
    public function showAssignStaff()
    {
        $staff = Staff::all();
        $classes = Classroom::all();

        return view("app.dashboard.admin.staff.assign")->with([
            "teachers" => $staff,
            "classes" => $classes
        ]);
    }

    public function showAddClass()
    {
        $grades = Grade::all();

        return view("app.dashboard.admin.class.add")->with("grades", $grades);
    }

    public function addClass(Request $request)
    {
        try {
            $this->addClassForm->validate($request->input());
        } catch (ValidationException $e) {
            return redirect()->back()->withInput()
                ->with('error', $e->getErrors());
        }

        $class = new Classroom($request->input());

        if ($class->save()) {
            return redirect()->back()->with(
                "message", [
                [
                    "type" => "success",
                    "title" => "Success!",
                    "message" => "Class was added successfully"
                ]
            ]);
        }
        return redirect()->back()->with(
            "error", [
                [
                    "type" => "error",
                    "title" => "Something wen't wrong!",
                    "message" => "Something wen't wrong while trying to process request. Please try again"
                ]
            ]
        )->withInput();

    }

    public function showAddStaff()
    {
        return view('app.dashboard.admin.staff.add');
    }

    public function handleAddStaff(Request $request)
    {
        try {
            $this->addTeacherForm->validate($request->input());
        } catch (ValidationException $e) {
            return redirect()->back()->withInput()
                ->with('errors', $e->getErrors());

        }

        if(User::whereEmail($request->input('email'))->count() != 0) {
            return redirect()->back()->withInput()
                ->with('errors', [
                    [
                        'type' => "warning",
                        "title" => "Duplicate entry",
                        "message" => "Another user with the same email already exists"
                    ]
                ]);
        }


        if (StaffModel::add($request->input())) {
            return redirect()->back()->with(
                "message", [
                [
                    "type" => "success",
                    "title" => "Success!",
                    "message" => "Teacher was added successfully"
                ]
            ]);
        }
        return redirect()->back()->with(
            "error", [
                [
                    "type" => "error",
                    "title" => "Something wen't wrong!",
                    "message" => "Something wen't wrong while trying to process request. Please try again"
                ]
            ]
        )->withInput();
    }

    /**
     * Returns the list staff page to the user
     * @return mixed
     */
    public function showListStaff()
    {
        $staff = Staff::paginate(10);

        return view("app.dashboard.admin.staff.list")->with("teachers", $staff);
    }


    /**
     * Returns the view staff member details page to the user
     * @param $id int
     * @return mixed
     */
    public function showStaffMember($id)
    {
        $id = (int)$id;

        $staff = Staff::find($id);

        if($staff->count() != 0) {

            return view("app.dashboard.admin.staff.single");

        } else {
            abort(404, "Staff member matching those details was not found");
        }
    }

    /**
     * Shows the add grade page to the user
     * @return mixed
     */
    public function showAddGrade()
    {
        return view("app.dashboard.admin.class.add-grade");
    }

    /**
     * Handles adding a new grade to the application
     * @param Request $request
     * @return mixed
     */
    public function handleAddGrade(Request $request)
    {
        try {
            $this->addGradeForm->validate($request->input());
        } catch (ValidationException $e) {
            return redirect()->back()->withInput()
                ->with('error', $e->getErrors());
        }

        $grade = new Grade($request->input());

        if ($grade->save()) {
            return redirect()->back()->with(
                "message", [
                [
                    "type" => "success",
                    "title" => "Success!",
                    "message" => "Grade was added successfully"
                ]
            ]);
        }
        return redirect()->back()->with(
            "error", [
                [
                    "type" => "error",
                    "title" => "Something wen't wrong!",
                    "message" => "Something wen't wrong while trying to prcess request. Please try again"
                ]
            ]
        )->withInput();

    }

    /**
     * Returns the list classes page to the user
     * @param Helper $helper
     * @return mixed
     */
    public function showListClasses(Helper $helper)
    {
        $classes = $helper->getClasses();

        return view("app.dashboard.admin.class.list")->with(
            'classes', $classes
        );
    }

    /**
     * Returns the add student page to the user
     * @param Helper $helper
     * @return mixed
     */
    public function showAddStudent(Helper $helper)
    {
        return view('app.dashboard.admin.students.add')->with([
            'classes' => $helper->getClasses()
        ]);
    }

    /**
     * Handles adding a student into the application
     * @param Request $request
     * @return mixed
     */
    public function handleAddStudent(Request $request)
    {
        try {
            $this->addStudentForm->validate($request->input());
        } catch (ValidationException $e) {
            return redirect()->back()->withInput()
                ->with('error', $e->getErrors());
        }

        $student = new Student($request->input());

        if ($student->save()) {
            Event::fire('student.Add');

            return redirect()->back()->with(
                "message", [
                [
                    "type" => "success",
                    "title" => "Success!",
                    "message" => "Student was registered successfully"
                ]
            ]);
        }
        return redirect()->back()->with(
            "error", [
                [
                    "type" => "error",
                    "title" => "Something wen't wrong!",
                    "message" => "Something wen't wrong while trying to prcess request. Please try again"
                ]
            ]
        )->withInput();

    }

    /**
     * Returns the list students page to the user
     * @param Student $student
     * @param Request $request
     * @param Helper $helper
     * @return mixed
     */
    public function showListStudents(Student $student, Request $request, Helper $helper)
    {
        // Quick sanity checks
        $perPage = 0;
        $students = null;

        if ($request->has('perPage')) {
            $perPage = (int)$request->input('perPage');
        }

        if ($perPage <= 0 || $perPage > 50) {
            $perPage = 10;
        }

        if ($request->has('s') || $request->has('class')) {
            if ($request->has('s') && $request->has('class')) {
                $query = $request->input('s');
                $class = (int)$request->input('class');

                if ($class == 0) {

                    if ($query != "") {
                        $students = $student->where('lastName', 'LIKE', "%{$query}%")->paginate($perPage);
                    } else {
                        $students = $student->paginate($perPage);
                    }
                } else {
                    if ($query != "") {
                        $students = $student->where('classId', '=', $class);

                        $students = $students->where('lastName', 'LIKE', "%{$query}%")->paginate($perPage);
                    } else {
                        $students = $student->where('classId', '=', $class);
                    }
                }
            } else {
                if ($request->has('s')) {
                    $query = $request->input('s');

                    $students = $student->where('lastName', 'LIKE', "%{$query}%")->paginate($perPage);
                }

                if ($request->has('class')) {
                    $class = (int)$request->input('class');

                    if ($class == 0) {
                        $students = $student->paginate($perPage);
                    } else {
                        $students = $student->where('classId', '=', $class)->paginate($perPage);
                    }
                }
            }
        } else {
            $students = $student->paginate($perPage);
        }


        return view('app.dashboard.admin.students.list')->with([
            'students' => $students,
            'classes' => $helper->getClasses()
        ]);
    }

    /**
     * Returns the attendance statistics page to the user
     * @return mixed
     */
    public function showAttendanceStatistics()
    {
        return view("app.dashboard.admin.attendance.statistics");
    }

    /**
     * Returns the settings page to the user
     * @return mixed
     */
    public function showSettings()
    {
        return view('app.dashboard.admin.settings');
    }

    /**
     * Returns the add fees page to the user
     * @param Helper $helper
     * @return mixed
     */
    public function showAddFees(Helper $helper)
    {
        return view('app.dashboard.admin.fees.add')->with([
            'classes' => $helper->getClasses()
        ]);
    }

    /**
     * Returns students matching the entered last name
     * @param Request $request
     * @return mixed
     */
    public function handleFindStudent(Request $request)
    {
        try {
            $this->findStudentForm->validate($request->input());
        } catch (ValidationException $e) {
            return redirect()->back()->withInput()
                ->with('error', $e->getErrors());
        }

        $student = Student::where("lastName", "LIKE", "%{$request->input('lastName')}%");

        $student = $student->where('classId', "=", $request->input("class"));

        if ($student->count() < 1) {
            return redirect()->back()->withInput()
                ->with('error', [
                    [
                        'title' => 'Student not found',
                        'message' => 'No student matching the entered information was found',
                        'type' => 'error'
                    ]
                ]);
        }

        return redirect()->back()->with('students', $student->get());
    }

    /**
     * Handles adding a new fees record
     * @param Request $request
     * @return mixed
     */
    public function handleAddFees(Request $request)
    {
        try {
            $this->feesPaymentForm->validate($request->input());
        } catch (ValidationException $e) {
            return redirect()->back()->withInput()
                ->with('error', $e->getErrors());
        }

        $payment = new Fees($request->input());

        if ($payment->save()) {
            return redirect()->back()->with(
                "message", [
                [
                    "type" => "success",
                    "title" => "Success!",
                    "message" => "Fees payment recorded successfully"
                ]
            ]);
        }
        return redirect()->back()->with(
            "error", [
                [
                    "type" => "error",
                    "title" => "Something wen't wrong!",
                    "message" => "Something wen't wrong while trying to prcess request. Please try again"
                ]
            ]
        )->withInput();
    }


    public function showStaffClockIns(Request $request)
    {
        $date = $request->has('date') ? $request->get('date') : (new Carbon())->toFormattedDateString();
        $today = date('Y-m-d');

        if($request->has("date") && !$request->has('ec')) {

            $members = Staff::all();
            $results = [];

            $d = new Carbon($request->get("date"));

            foreach ($members as $staff)
            {
                $staffAttendance = StaffAttendance::where([
                    ['staffId', '=', $staff->id],
                    ['date', '=', $date],
                ]);

                if($staffAttendance->count() !== 0) {
                    $staffAttendance = $staffAttendance->get()->first();

                    $enterText = new Carbon($staffAttendance->clockInTime);

                    if($staffAttendance->clockOutTime !== "0000-00-00 00:00:00") {
                        $leaveText = new Carbon($staffAttendance->clockOutTime);
                    } else {
                        $leaveText = "Did not check out";
                    }
                } else {
                    $enterText = "Did not check in";
                    $leaveText = "Did not check out";
                }


                $results[] = (object)[
                    "name" => sprintf("%s %s", $staff->firstNames, $staff->lastName),
                    "enter" => $enterText instanceof Carbon ? $enterText->toTimeString() : $enterText,
                    "leave" => $leaveText instanceof Carbon ? $leaveText->toTimeString() : $leaveText,
                ];
            }

            return view('app.dashboard.admin.staff.staff-attendance')->with([
                'records' => $results,
                "date" => $d->toFormattedDateString(),
                "isSingular" => true,
                "isSingularAndSingleDate" => false,
                "isSingleDate" => true
            ]);

        } else {

            $isSingular = $request->has('ec');
            $isSingularAndSingleDate = $request->has("date");

            if($isSingular) {

                if($isSingularAndSingleDate) {

                    $date = new Carbon($request->get("date"));
                    $dateText = $date->toFormattedDateString();

                    $ecNumber = strtolower($request->input('ec'));

                    $staff = Staff::where("ecNumber", $ecNumber);

                    if($staff->count() == 0) {

                        return redirect()->to("/app/admin/staff/clock-ins")->withInput()
                            ->with('errors', [
                                [
                                    "title" => "Staff member not found",
                                    "message" => "No staff member matching that EC number was found. Please recheck it and try again",
                                    "type" => "warning"
                                ]
                            ]);
                    } else {
                        $staff = $staff->get()->first();
                    }


                    $records = StaffAttendance::where([
                        ['staffId', '=', $staff->id],
                        ["date", "=", $date->toDateString()]
                    ]);

                    if($records->count() !== 0) {
                        $results = new Collection();
                        $records = $records->get();

                        foreach ($records as $record)
                        {
                            $results[] = (object)[
                                "date" => (new Carbon($record->date))->toFormattedDateString(),
                                "enterText" => $record->clockInTime != "" ? (new Carbon($record->clockInTime))->toTimeString() : "Did not check in",
                                "leaveText" => $record->clockOutTime != "" ? (new Carbon($record->clockOutTime))->toTimeString() : "Did not check out",
                            ];
                        }

                        return view('app.dashboard.admin.staff.staff-attendance')->with([
                            'records' => $results,
                            "isSingular" => true,
                            "recordsFound" => true,
                            "date" => $dateText,
                            "resultSet" => $results->toJson(),
                            "isSingularAndSingleDate" => true,
                            "isSingleDate" => false,
                            "staff" => sprintf('%s %s', $staff->firstNames, $staff->lastName),
                        ]);

                    } else {
                        return redirect()->to("/app/admin/staff/clock-ins")->withInput()
                            ->with('errors', [
                                [
                                    "title" => "No records found",
                                    "message" => "No attendance records were found for " . sprintf('%s %s',
                                            $staff->firstNames, $staff->lastName),
                                    "type" => "warning"
                                ]
                            ]);
                        }
                    } else {

                    $ecNumber = strtolower($request->input('ec'));

                    $staff = Staff::where("ecNumber", $ecNumber);

                    if($staff->count() == 0) {

                        return redirect()->back()->withInput()
                            ->with('errors', [
                                [
                                    "title" => "Staff member not found",
                                    "message" => "No staff member matching that EC number was found. Please recheck it and try again",
                                    "type" => "warning"
                                ]
                            ]);

                    } else {
                        $staff = $staff->get()->first();
                    }


                    $records = StaffAttendance::where([
                        ['staffId', '=', $staff->id]
                    ]);

                    if($records->count() !== 0) {
                        $results = new Collection();
                        $records = $records->get();

                        foreach ($records as $record)
                        {
                            $results[] = (object)[
                                "date" => (new Carbon($record->date))->toFormattedDateString(),
                                "enterText" => $record->clockInTime != "" ? (new Carbon($record->clockInTime))->toTimeString() : "Did not check in",
                                "leaveText" => $record->clockOutTime != "" ? (new Carbon($record->clockOutTime))->toTimeString() : "Did not check out",
                            ];
                        }

                        return view('app.dashboard.admin.staff.staff-attendance')->with([
                            'records' => $results,
                            "isSingular" => true,
                            "recordsFound" => true,
                            "isSingleDate" => false,
                            "resultSet" => $results->toJson(),
                            "isSingularAndSingleDate" => false,
                            "staff" => sprintf('%s %s', $staff->firstNames, $staff->lastName),
                        ]);

                    } else {
                        return redirect()->to("/app/admin/staff/clock-ins")->withInput()
                            ->with('errors', [
                                [
                                    "title" => "No records found",
                                    "message" => "No attendance records were found for " . sprintf('%s %s', $staff->firstNames, $staff->lastName),
                                    "type" => "warning"
                                ]
                            ]);
                    }

                }

            } else {
                $members = Staff::all();
                $results = [];

                foreach ($members as $staff)
                {
                    $staffAttendance = StaffAttendance::where([
                        ['staffId', '=', $staff->id],
                        ['date', '=', $today],
                    ]);

                    $in = StaffAttendance::where([
                            ['staffId', '=', $staff->id],
                            ['date', '=', $today],
                            ['clockOutTime', '=', '0000-00-00 00:00:00']
                        ])->count() !== 0;

                    if($staffAttendance->count() !== 0) {
                        $staffAttendance = $staffAttendance->get()->first();

                        $enterText = new Carbon($staffAttendance->clockInTime);

                        if($staffAttendance->clockOutTime !== "0000-00-00 00:00:00") {
                            $leaveText = new Carbon($staffAttendance->clockOutTime);
                        } else {
                            $leaveText = "Has not checked out";
                        }
                    } else {
                        $enterText = "Has not checked in";
                        $leaveText = "Has not checked out";
                    }


                    $results[] = (object)[
                        "name" => sprintf("%s %s", $staff->firstNames, $staff->lastName),
                        "available" => $in,
                        "enter" => $enterText instanceof Carbon ? $enterText->toTimeString() : $enterText,
                        "leave" => $leaveText instanceof Carbon ? $leaveText->toTimeString() : $leaveText,
                    ];

                }

                return view('app.dashboard.admin.staff.staff-attendance')->with([
                    'records' => $results,
                    "date" => $date,
                    "isSingleDate" => false,
                    "isSingular" => false
                ]);
            }
        }
    }

    public function handleFetchUserClockIns($id, Request $request)
    {
        if(!$request->ajax()) {
            // TODO: Laravel throw exception
            logger("Ajax url visited directly. Client IP address: '{$request->ip()}'");
             abort(404);
        }

        $staff = Staff::findOrFail($id);

        $staffAttendance = StaffAttendance::where([
           ['staffId', '=', $staff->id]
        ])->get()->toJson();

        return response($staffAttendance, 200, [
            "Content-Type" => "application/json"
        ]);
    }
}

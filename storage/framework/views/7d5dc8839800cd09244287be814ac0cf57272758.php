<script src="<?php echo e(asset('assets/vendor/chartist-js/chartist.min.js')); ?>"></script>
<script src="<?php echo e(asset('assets/vendor/chartist-plugin-tooltip/chartist-plugin-tooltip.min.js')); ?>"></script>
<script src="<?php echo e(asset('assets/vendor/jvectormap/jquery-jvectormap.min.js')); ?>"></script>
<script src="<?php echo e(asset('assets/vendor/jvectormap/maps/jquery-jvectormap-world-mill-en.js')); ?>"></script>
<script src="<?php echo e(asset('assets/vendor/matchheight/jquery.matchHeight-min.js')); ?>"></script>
<script src="<?php echo e(asset('assets/vendor/peity/jquery.peity.min.js')); ?>"></script>
<script src="<?php echo e(asset('assets/vendor/formvalidation/formValidation.min.js')); ?>"></script>
<script src="<?php echo e(asset('assets/vendor/formvalidation/framework/bootstrap.min.js')); ?>"></script>
<script src="<?php echo e(asset('assets/vendor/bootstrap-datepicker/bootstrap-datepicker.min.js')); ?>"></script>
<script src="<?php echo e(asset('assets/js/sweetalert.min.js')); ?>"></script>


<script src="<?php echo e(asset('assets/js/forms/validation.min.js')); ?>"></script>
<script src="<?php echo e(asset('assets/js/components/bootstrap-datepicker.min.js')); ?>"></script>
<script src="<?php echo e(asset('assets/js/dashboard/v1.min.js')); ?>"></script>
<script src="<?php echo e(asset('assets/js/dashboard.js')); ?>"></script>

<script type="text/javascript">
    $("#btnLogout").click(function () {
        window.location = "<?php echo e(url('/logout')); ?>";
    });
    $("#btnLock").click(function () {
        window.location = "<?php echo e(url('/app/lock')); ?>";
    });
    $("#btnSettings").click(function () {
        window.location = "<?php echo e(url('/app/admin/settings')); ?>";
    });
</script>

<script type="text/javascript">
    $(document).ready(function () {
        <?php echo $__env->make('app.partials.swal', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>

    });
</script>

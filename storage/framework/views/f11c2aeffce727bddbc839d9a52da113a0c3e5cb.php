<!DOCTYPE html>
<html class="no-js css-menubar" lang="en" token="<?php echo e(csrf_token()); ?>" id="siteRoot">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta http-equiv="content-type" content="text/html;charset=utf-8"/>
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">
    <meta name="description" content="bootstrap material admin template">
    <meta name="author" content="">

    <title><?php echo $__env->yieldContent('title'); ?></title>

    <link rel="apple-touch-icon" href="<?php echo e(asset('assets/images/apple-touch-icon.png')); ?>">
    <link rel="shortcut icon" href="<?php echo e(asset('assets/images/favicon.ico')); ?>">

    <!-- Stylesheets -->
    <link rel="stylesheet" href="<?php echo e(asset('assets/css/bootstrap.min3f0d.css')); ?>">
    <link rel="stylesheet" href="<?php echo e(asset('assets/css/bootstrap-extend.min3f0d.css')); ?>">
    <link rel="stylesheet" href="<?php echo e(asset('assets/css/site.min3f0d.css')); ?>">

    <!-- Plugins -->
    <link rel="stylesheet" href="<?php echo e(asset('assets/vendor/animsition/animsition.min3f0d.css')); ?>">
    <link rel="stylesheet" href="<?php echo e(asset('assets/vendor/asscrollable/asScrollable.min3f0d.css')); ?>">
    <link rel="stylesheet" href="<?php echo e(asset('assets/vendor/switchery/switchery.min3f0d.css')); ?>">
    <link rel="stylesheet" href="<?php echo e(asset('assets/vendor/intro-js/introjs.min3f0d.css')); ?>">
    <link rel="stylesheet" href="<?php echo e(asset('assets/vendor/slidepanel/slidePanel.min3f0d.css')); ?>">
    <link rel="stylesheet" href="<?php echo e(asset('assets/vendor/flag-icon-css/flag-icon.min3f0d.css')); ?>">
    <link rel="stylesheet" href="<?php echo e(asset('assets/vendor/waves/waves.min3f0d.css')); ?>">
    <link rel="stylesheet" href="<?php echo e(asset('assets/vendor/toastr/toastr.min3f0d.css')); ?>">
    <link rel="stylesheet" href="<?php echo e(asset('assets/css/global-custom.css')); ?>">

    <!-- Page Level Styles -->
    <?php echo $__env->yieldContent('page-styles'); ?>

            <!-- Fonts -->
    <link rel="stylesheet" href="<?php echo e(asset('assets//fonts/material-design/material-design.min3f0d.css')); ?>">
    <link rel="stylesheet" href="<?php echo e(asset('assets//fonts/brand-icons/brand-icons.min3f0d.css')); ?>">

    <link rel='stylesheet' href='http://fonts.googleapis.com/css?family=Roboto:400,400italic,700'>


    <!--[if lt IE 9]>
    <script src="<?php echo e(asset('assets/vendor/html5shiv/html5shiv.min.js')); ?>"></script>
    <![endif]-->

    <!--[if lt IE 10]>
    <script src="<?php echo e(asset('assets/vendor/media-match/media.match.min.js')); ?>"></script>
    <script src="<?php echo e(asset('assets/vendor/respond/respond.min.js')); ?>"></script>
    <![endif]-->

    <!-- Scripts -->
    <script src="<?php echo e(asset('assets/vendor/modernizr/modernizr.min.js')); ?>"></script>
    <script src="<?php echo e(asset('assets/vendor/breakpoints/breakpoints.min.js')); ?>"></script>
    <script>
        Breakpoints();
    </script>
</head>

<body class="<?php echo $__env->yieldContent('body-styles'); ?>">
<!--[if lt IE 8]>
<p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a
        href="https://google.com/chrome/">upgrade your browser</a> to improve your experience.</p>
<![endif]-->

<!-- Page Content -->
<?php echo $__env->yieldContent('content'); ?>

        <!-- End Page Content -->

<!-- Global JS -->

<!-- Core  -->
<script src="<?php echo e(asset('assets/vendor/jquery/jquery.min.js')); ?>"></script>
<script src="<?php echo e(asset('assets/vendor/bootstrap/bootstrap.min.js')); ?>"></script>
<script src="<?php echo e(asset('assets/vendor/animsition/animsition.min.js')); ?>"></script>
<script src="<?php echo e(asset('assets/vendor/asscroll/jquery-asScroll.min.js')); ?>"></script>
<script src="<?php echo e(asset('assets/vendor/mousewheel/jquery.mousewheel.min.js')); ?>"></script>
<script src="<?php echo e(asset('assets/vendor/asscrollable/jquery.asScrollable.all.min.js')); ?>"></script>
<script src="<?php echo e(asset('assets/vendor/ashoverscroll/jquery-asHoverScroll.min.js')); ?>"></script>
<script src="<?php echo e(asset('assets/vendor/waves/waves.min.js')); ?>"></script>

<!-- Plugins -->
<script src="<?php echo e(asset('assets/vendor/switchery/switchery.min.js')); ?>"></script>
<script src="<?php echo e(asset('assets/vendor/intro-js/intro.min.js')); ?>"></script>
<script src="<?php echo e(asset('assets/vendor/screenfull/screenfull.min.js')); ?>"></script>
<script src="<?php echo e(asset('assets/vendor/slidepanel/jquery-slidePanel.min.js')); ?>"></script>
<script src="<?php echo e(asset('assets/js/core.min.js')); ?>"></script>
<script src="<?php echo e(asset('assets/js/site.min.js')); ?>"></script>
<script src="<?php echo e(asset('assets/js/sections/menu.min.js')); ?>"></script>
<script src="<?php echo e(asset('assets/js/sections/menubar.min.js')); ?>"></script>
<script src="<?php echo e(asset('assets/js/sections/gridmenu.min.js')); ?>"></script>
<script src="<?php echo e(asset('assets/js/sections/sidebar.min.js')); ?>"></script>
<script src="<?php echo e(asset('assets/js/configs/config-colors.min.js')); ?>"></script>
<script src="<?php echo e(asset('assets/js/configs/config-tour.min.js')); ?>"></script>

<script src="<?php echo e(asset('assets/js/components/asscrollable.min.js')); ?>"></script>
<script src="<?php echo e(asset('assets/js/components/animsition.min.js')); ?>"></script>
<script src="<?php echo e(asset('assets/js/components/slidepanel.min.js')); ?>"></script>
<script src="<?php echo e(asset('assets/js/components/switchery.min.js')); ?>"></script>
<script src="<?php echo e(asset('assets/js/components/tabs.min.js')); ?>"></script>
<script src="<?php echo e(asset('assets/js/components/jquery-placeholder.min.js')); ?>"></script>
<script src="<?php echo e(asset('assets/js/components/material.min.js')); ?>"></script>
<script src="<?php echo e(asset('assets/js/components/toastr.min.js')); ?>"></script>

<script>
    (function (document, window, $) {
        'use strict';

        var Site = window.Site;
        $(document).ready(function () {
            Site.run();
        });
    })(document, window, jQuery);
</script>
<!-- End Global JS -->

<!-- Page Level Scripts -->
<?php echo $__env->yieldContent('page-scripts'); ?>
        <!-- End page level scripts -->

<!-- Google Analytics -->
<script>
    <!-- TODO: Implement Google Analytics into page -->
</script>
</body>
</html>

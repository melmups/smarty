<!-- Footer -->
<footer class="site-footer">
    <div class="site-footer-legal">© <?php echo 2017; ?> <a href="https://alcratetch.com">Alcra Technologies</a></div>
    <div class="site-footer-right">
        Crafted with <i class="red-600 icon md-favorite"></i> by <a href="https://alcratetch.com">Alcra Tech.</a>
    </div>
</footer>
<!-- End footer -->

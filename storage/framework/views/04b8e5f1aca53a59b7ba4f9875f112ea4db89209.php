<?php if(Session::has('errors')): ?>
    <?php if(is_array(Session::get('errors'))): ?>
        <?php foreach(Session::get('errors') as $error): ?>
            <div class="alert alert-<?php echo e($error['type']); ?> alert-dismissible" role="alert">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <strong><?php echo e($error['title']); ?></strong><br><?php echo e($error['message']); ?>

            </div>
        <?php endforeach; ?>
    <?php elseif(Session::get('errors') instanceof \Illuminate\Support\MessageBag): ?>
        <?php foreach(Session::get('errors')->getMessages() as $error): ?>
            <div class="alert alert-<?php echo e($error['type']); ?> alert-dismissible" role="alert">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <strong><?php echo e($error['title']); ?></strong><br><?php echo e($error['message']); ?>

            </div>
        <?php endforeach; ?>
    <?php endif; ?>
<?php endif; ?>

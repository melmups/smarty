<?php if(Session::has('message')): ?>
    <?php if(is_array(Session::get('message'))): ?>
        <?php foreach(Session::get('message') as $message): ?>
            swal(
            "<?php echo e($message['title']); ?>",
            "<?php echo e($message['message']); ?>",
            "<?php echo e($message['type']); ?>"
            );
        <?php endforeach; ?>
    <?php else: ?>
        swal("<?php echo e(Session::get('message')); ?>")
    <?php endif; ?>
<?php endif; ?>

<?php if(Session::has('error')): ?>
    <?php if(is_array(Session::get('error'))): ?>
        <?php foreach(Session::get('error') as $error): ?>
            swal(
            "<?php echo e($error['title']); ?>",
            "<?php echo e($error['message']); ?>",
            "<?php echo e("error"); ?>"
            );
        <?php endforeach; ?>
    <?php else: ?>
        <?php foreach(json_decode(Session::get('error')) as $element): ?>
            swal("<?php echo e($element[0]); ?>")
        <?php endforeach; ?>
    <?php endif; ?>
<?php endif; ?>

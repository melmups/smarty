<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateStaffTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('staff', function (Blueprint $table) {
            $table->increments('id');
            $table->string('firstNames');
            $table->string('lastName');
            $table->enum('gender', ['male', 'female']);
            $table->date('dateOfBirth');
            $table->string('nationalId');
            $table->string('ecNumber');
            $table->string('nextOfKin');
            $table->string('phone');
            $table->string('homeAddress');
            $table->integer('staffId');
            $table->enum('type', ['full-time', 'student']);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('staff');
    }
}

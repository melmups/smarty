<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateFeesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create("fees_payments", function (Blueprint $table) {
            $table->increments("id");
            $table->string("recieptId");
            $table->date("paymentDate");
            $table->float("amount");
            $table->integer("studentId");
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop("fees_payments");
    }
}

<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateStudentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('students', function (Blueprint $table) {
            $table->increments('id');
            $table->string('firstNames');
            $table->string('lastName');
            $table->enum('gender', ['male', 'female']);
            $table->date('dateOfBirth');
            $table->string('birthEntryNo');
            $table->string('residentialArea');
            $table->string('guardianName');
            $table->string('guardianPhone');
            $table->string('medicalConditions');
            $table->integer('classId');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('students');
    }
}

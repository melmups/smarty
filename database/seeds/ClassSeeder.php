<?php

use Smarty\Data\Seeds\BaseSeeder;
use Faker\Factory;

class ClassSeeder extends BaseSeeder
{
    /**
     * Number of classes to create
     * @var int
     */
    protected $count = 18;

    /**
     * Number of classes per grade
     * @var int
     */
    protected $gradeCount = 3;

    /**
     * The table to seed
     * @var string
     */
    protected $table = 'classes';

    /**
     * Returns a list of classes
     * @return mixed Array
     */
    public function getData()
    {
        $faker = Factory::create();

        $data = [];


        $loop = $this->count / $this->gradeCount;

        for ($i = 0; $i < $loop; $i++)
        {

        }

        return $data;
    }
}
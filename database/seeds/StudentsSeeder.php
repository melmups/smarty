<?php

use Smarty\Data\Seeds\BaseSeeder;
use Faker\Factory;

class StudentSeeder extends BaseSeeder
{

    protected $table = 'students';

    /**
     * Returns the default application settings
     * @return mixed Array
     */
    public function getData()
    {
        $faker = Factory::create();

        $sexes = ['male', 'female'];
        $conditions = ['', 'Asthma', 'HIV'];

        $data = [];

        for ($i = 0; $i < 1001; $i++) {
            $data[] = [
                'firstNames' => $faker->firstName,
                'lastName' => $faker->lastName,
                'gender' => $sexes[array_rand($sexes)],
                'dateOfBirth' => $faker->date,
                'birthEntryNo' => $faker->randomNumber,
                'residentialArea' => $faker->address,
                'guardianName' => $faker->name,
                'guardianPhone' => $faker->phoneNumber,
                'medicalConditions' => $conditions[array_rand($conditions)],
                "classId" => rand(1, 18)
            ];
        }

        return $data;
    }
}

function redrawContainerLogo()
{
    var bg = $('#logo-background');
    var parent = bg.parent();

    bg.attr('width', parent.width() / 2);
    bg.attr('height', parent.height() / 2);
}

$(document).on('ready', function(){
    new Chart(document.getElementById("chartGirlToBoyRatio").getContext("2d")).Pie(pieData);

    redrawContainerLogo();
});


$(window).on('resize', function() {
   redrawContainerLogo();
});
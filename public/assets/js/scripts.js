
jQuery(document).ready(function() {

    /*
        Form
    */
    $('.registration-form fieldset:first-child').fadeIn('slow');


    $('.registration-form input[type="text"], .registration-form input[type="password"], .registration-form textarea').on('focus', function() {
    	$(this).removeClass('input-error');
    });

    // next step
    $('.registration-form .btn-next').on('click', function() {
    	var parent_fieldset = $(this).parents('fieldset');

		var grades = window.setupInformation.grades;
		var classes = window.setupInformation.classes;

		var next_step = true;

    	if(parent_fieldset.hasClass('add-grade-form')) {
			if(grades === 0) {
				swal("No grades", "You need to add at least one grade to continue", "warning");
				next_step = false;
			}
		} else if(parent_fieldset.hasClass('add-class-form')) {
			if(classes === 0) {
				swal("No classes", "You need to add at least one class to continue", "warning");
				next_step = false;
			}
		}
    	
    	if( next_step ) {
    		parent_fieldset.fadeOut(400, function() {
	    		$(this).next().fadeIn();
	    	});
    	}
    });
    
    // previous step
    $('.registration-form .btn-previous').on('click', function() {
    	$(this).parents('fieldset').fadeOut(400, function() {
    		$(this).prev().fadeIn();
    	});
    });
    
    // submit
    $('.btn-add-grade-submit').on('submit', function(e) {
    	
    	$(this).find('input[type="text"], input[type="password"], textarea').each(function() {
    		if( $(this).val() == "" ) {
    			e.preventDefault();
    			$(this).addClass('input-error');
    		}
    		else {
    			$(this).removeClass('input-error');
    		}
    	});
    	
    });
    
    
});

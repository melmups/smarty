
<!DOCTYPE html>
<html>
<head>
    <title>Clock-In | Smarty</title>

    <link rel="stylesheet" href="{{ asset('assets/css/bootstrap.min3f0d.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/css/bootstrap-extend.min3f0d.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/css/site.min3f0d.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/css/sweetalert.css') }}">

    <style>

        html, body {
            height: 100%;
            background: #000;
            color: #000;
        }

        .clockInform {
            padding-top: 15px;
            margin: auto;
        }

        .content {
            width: 400px;
            height: 400px;
        }

        .formMiddle {
            position: relative;
            top: 50%;
            transform: translateY(-50);
            border-radius: 8px;
            padding: 25px;
            background-color: rgba(200, 200, 200, .75);
        }
        
        #submit-buttons {
            margin: 10px;
            height: 30px;
        }

        @media only screen and (max-width: 1000px)
        {
            .content {
                width: 100%;
            }
        }
    </style>
</head>
<body>
<div class="container">
    <div class="row">
        <div class="col-md-4"></div>
        <div class="col-md-4 content">
            <div class="formMiddle">
                <h2>Staff Check-In</h2>
                <hr>

                <div class="errors">
                    @include('app.partials.validation-error')
                </div>

                <form class="clockInform" method="post">
                    {!! csrf_field() !!}
                    <div class="form-group">
                        <label class="control-label" for="txtTeacherECNumber">Enter EC Number</label>
                        <input type="text" id="txtTeacherECNumber"name="teacherEcNumber" class="form-control" required>
                    </div>
                    <div class="form-group" id="submit-buttons">
                        <button type="submit" name="check-in" value="true" class="btn btn-lg btn-success pull-left">Check In</button>
                        <button type="submit" name="check-out" value="true" class="btn btn-lg btn-warning pull-right">Check Out</button>
                    </div>
                </form>
            </div>
        </div>
        <div class="col-md-4"></div>
    </div>
</div>
<script src="{{ asset('assets/js/jquery-1.11.1.js') }}"></script>
<script src="{{ asset('assets/vendor/bootstrap/bootstrap.min.js') }}"></script>
<script src="{{ asset('assets/js/sweetalert.min.js') }}"></script>
</body>
</html>


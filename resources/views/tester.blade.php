<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="{{ asset('assets/css/bootstrap.min3f0d.css')  }}">
    <title>Document</title>
</head>
<body>
<div class="container">
    <div class="row">
        <br>
        <table class="table table-hover table-striped table-bordered">
            <thead>
            <tr>
                <th>Field</th>
                <th>Value</th>
            </tr>
            </thead>
            <tbody>
            <tr>
                <td>First Term Start Date</td>
                <td>{{ $terms->getFirstTermStartDate() }}</td>
            </tr>
            <tr>
                <td>First Term End Date</td>
                <td>{{ $terms->getFirstTermEndDate() }}</td>
            </tr>
            <tr>
                <td>Second Term Start Date</td>
                <td>{{ $terms->getSecondTermStartDate() }}</td>
            </tr>
            <tr>
                <td>Second Term End Date</td>
                <td>{{ $terms->getSecondTermEndDate() }}</td>
            </tr>
            <tr>
                <td>Third Term Start Date</td>
                <td>{{ $terms->getThirdTermStartDate() }}</td>
            </tr>
            <tr>
                <td>Third Term End Date</td>
                <td>{{ $terms->getThirdTermEndDate() }}</td>
            </tr>

            </tbody>
        </table>
        <hr>
        Current Term: {{ var_dump($terms->getCurrentTerm()) }}
    </div>
</div>
</body>
</html>
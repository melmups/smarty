@extends('app.master')

@section('title')
    Login | Smarty
@endsection

@section('page-styles')
    <link rel="stylesheet" href="{{ asset('assets/css/pages/login.min3f0d.css') }}" media="screen" title="no title"
          charset="utf-8">

    <style media="screen">
        .page-login:before {
            background-image: url('{{ asset('assets/images/login.jpg') }}')
        }
    </style>
@endsection

@section('body-styles')
    page-login layout-full page-dark
    @endsection

    @section('content')
            <!-- Page -->
    <div class="page animsition vertical-align text-center" data-animsition-in="fade-in"
         data-animsition-out="fade-out">
        <div class="page-content vertical-align-middle">
            <div class="brand">
                <img class="brand-img" src="{{ asset('assets/images/logo.png') }}" alt="...">

                <h2 class="brand-text">Smarty</h2>
            </div>
            <p>Sign into your account</p>
            <form method="post" action="{{ url('/login') }}" id="frmLogin" autocomplete="off">
                <div id="alerts">
                    @include('app.partials.error')
                    @include('app.partials.message')
                </div>

                <input type="hidden" name="_token" value="{{ csrf_token() }}">

                <div class="form-group form-material floating">
                    <input type="email" class="form-control empty" id="inputEmail" name="email" value="" required="">
                    <label class="floating-label" for="inputEmail">Email</label>
                </div>
                <div class="form-group form-material floating">
                    <input type="password" class="form-control empty" id="inputPassword" name="password" required="">
                    <label class="floating-label" for="inputPassword">Password</label>
                </div>
                <div class="form-group clearfix">
                    <div class="checkbox-custom checkbox-inline checkbox-primary pull-left">
                        <input type="checkbox" id="inputCheckbox" name="remember">
                        <label for="inputCheckbox">Remember me</label>
                    </div>
                    <a class="pull-right" href="{{ url('/app/forgot-pass') }}">Forgot password?</a>
                </div>
                <button type="submit" class="btn btn-primary btn-block">Sign in</button>
            </form>

            <footer class="page-copyright page-copyright-inverse">
                <p>Alcra Technologies © @year. All rights Reserved.</p>

                <div class="social">
                    <a href="javascript:void(0)">
                        <i class="icon bd-twitter" aria-hidden="true"></i>
                    </a>
                    <a href="javascript:void(0)">
                        <i class="icon bd-facebook" aria-hidden="true"></i>
                    </a>
                    <a href="javascript:void(0)">
                        <i class="icon bd-dribbble" aria-hidden="true"></i>
                    </a>
                </div>
            </footer>
        </div>
    </div>
    <!-- End Page -->
@endsection

@section('page-scripts')
@endsection

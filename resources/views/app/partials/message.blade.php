@if(Session::has('messages'))
    @foreach(Session::get('messages') as $msg)
        <div class="alert alert-{{ $msg['type'] }} alert-dismissible" role="alert">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
            <strong>{{ $msg['title'] }}</strong><br>{{ $msg['message'] }}
        </div>
    @endforeach
@endif
@if(Session::has('message'))
    @if(is_array(Session::get('message')))
        @foreach(Session::get('message') as $message)
            swal(
            "{{ $message['title'] }}",
            "{{ $message['message'] }}",
            "{{ $message['type'] }}"
            );
        @endforeach
    @else
        swal("{{ Session::get('message') }}")
    @endif
@endif

@if(Session::has('error'))
    @if(is_array(Session::get('error')))
        @foreach(Session::get('error') as $error)
            swal(
            "{{ $error['title'] }}",
            "{{ $error['message'] }}",
            "{{ "error" }}"
            );
        @endforeach
    @else
        @foreach (json_decode(Session::get('error')) as $element)
            swal("{{ $element[0] }}")
        @endforeach
    @endif
@endif

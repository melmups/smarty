@if(Session::has('errors'))
    @if(is_array(Session::get('errors')))
        @foreach(Session::get('errors') as $error)
            <div class="alert alert-{{ $error['type']  }} alert-dismissible" role="alert">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <strong>{{ $error['title'] }}</strong><br>{{ $error['message'] }}
            </div>
        @endforeach
    @elseif(Session::get('errors') instanceof \Illuminate\Support\MessageBag)
        @foreach(Session::get('errors')->getMessages() as $error)
            <div class="alert alert-{{ $error['type']  }} alert-dismissible" role="alert">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <strong>{{ $error['title'] }}</strong><br>{{ $error['message'] }}
            </div>
        @endforeach
    @endif
@endif

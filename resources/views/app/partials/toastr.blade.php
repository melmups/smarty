<div class="hidden" aria-live="polite" data-plugin="toastr"
     data-message="{% $message %}"
     data-container-id="toast-top-right" data-position-class="toast-top-right"
     data-icon-class="toast-just-text toast-info toast-flat" role="alert">
</div>

@if(session('errors'))
    @foreach(session('errors')->getMessages() as $e)
        <div class="alert alert-danger alert-dismissible" role="alert">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
            <strong>{{ $e['title'] }}</strong> {{ $e['message'] }}
        </div>
    @endforeach
@endif
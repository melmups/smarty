@extends('app.master')

@section('title') Setup your copy of Smarty | Smarty @endsection

@section('page-styles')
    @include('app.dashboard.partials.styles')
    <link rel="stylesheet" href="{{ asset('assets/css/pages/register.min3f0d.css') }}" media="screen">
    <style media="screen">
        .text-white {
            text-align: justify;
            color: white;
        }
    </style>
@endsection

@section('body-styles')page-register layout-full page-dark @endsection

@section('content')
    <div class="page animsition vertical-align text-center" data-animsition-in="fade-in"
         data-animsition-out="fade-out">
        <div class="page-content vertical-align-middle">
            <div class="brand">
                <img class="brand-img" src="{{ asset('assets/images/logo.png') }}" alt="...">

                <h2 class="brand-text"> Smarty</h2>
            </div>

            <p>Fill in the following form to finish setting up your copy of Smarty</p>

           @include('app.partials.validation-error')

            <form method="post" role="form" id="setupForm" enctype="multipart/form-data" autocomplete="off">
                <input type="hidden" name="_token" value="{!! csrf_token() !!}">
                <br>
                <h4 class="title text-white">School Information</h4>

                <div class="form-group form-material floating">
                    <input type="text" placeholder="" class="form-control @if(!is_null(old('schoolName'))){{ "empty" }}@endif" value="{{ old('schoolName') }}"
                           id="txtSchoolName" name="schoolName" tabindex="-1" required="">
                    <label class="floating-label" for="txtSchoolName">School Name</label>
                </div>

                <div class="form-group form-material floating">
                    <select name="schoolType" class="form-control @if(!is_null(old('schoolType'))){{ "empty" }}@endif" id="slctSchoolType" required>
                        <option value=""></option>
                        @if(!old('schoolType'))
                            <option value="primary">Primary School</option>
                            <option value="secondary">Secondary School</option>
                        @else
                            @if(old('schoolType') == "primary")
                                <option value="primary" selected>Primary School</option>
                            @else
                                <option value="secondary" selected>Secondary School</option>
                            @endif
                        @endif
                    </select>
                    <label class="floating-label" for="slctSchoolType">School Type</label>
                </div>

                <div class="form-group form-material floating">
                    <input type="text" placeholder="" class="form-control @if(!is_null(old('schoolAddress'))){{ "empty" }}@endif" id="txtSchoolAddress"
                           value="{{ old('schoolAddress') }}" name="schoolAddress" tabindex="1" required="">
                    <label class="floating-label" for="txtSchoolAddress">School Address</label>
                </div>

                <div class="form-group form-material floating">
                    <input type="text" placeholder="" class="form-control @if(!is_null(old('schoolPhone'))){{ "empty" }}@endif" id="txtSchoolPhone"
                           value="{{ old('schoolPhone') }}" name="schoolPhone" tabindex="2" required="">
                    <label class="floating-label" for="txtSchoolPhone">School Phone</label>
                </div>

                <div class="form-group form-material floating">
                    <input type="text" class="form-control" placeholder="" readonly="" tabindex="3" required=""/>
                    <input type="file" id="inptLogo" accept="" name="schoolLogo" multiple=""/>
                    <label class="floating-label" for="inptLogo">School Logo</label>
                    @if(old("schoolName"))
                        <span class="help-block text-danger">Please re-upload the school logo</span>
                    @endif
                </div>

                <br>
                <h4 class="title text-white">Administrator Information</h4>

                <div class="form-group form-material floating">
                    <input type="text" placeholder="" class="form-control @if(!is_null(old('adminName'))){{ "empty" }}@endif" value="{{ old('adminName') }}"
                           id="txtAdminName" name="adminName" tabindex="4" required="">
                    <label class="floating-label" for="inputName">Administrator's full name</label>
                </div>
                <div class="form-group form-material floating">
                    <input type="email" placeholder="" class="form-control @if(!is_null(old('adminEmail'))){{ "empty" }}@endif" id="txtAdminEmail"
                           value="{{ old('adminEmail') }}" name="adminEmail" tabindex="5" required="">
                    <label class="floating-label" for="inputEmail">Administrator's email</label>
                </div>
                <div class="form-group form-material floating">
                    <input type="password" class="form-control empty" id="txtAdminPassword" name="adminPassword"
                           tabindex="6" required="">
                    <label class="floating-label" for="txtAdminPassword">Password</label>
                    @if(old("schoolName"))
                        <span class="help-block text-danger">Please re-enter password</span>
                    @endif
                </div>
                <div class="form-group form-material floating repeatpass">
                    <input type="password" class="form-control empty" id="txtRePassword" name="adminRePassword" tabindex="7"
                           required="">
                    <label class="floating-label" for="txtRePassword">Retype Password</label>
                    @if(old("schoolName"))
                        <span class="help-block text-danger">Please re-enter password confirmation</span>
                    @endif
                </div>
                <button type="submit" class="btn btn-primary btn-block" tabindex="8">Register</button>
            </form>
        </div>
    </div>
@endsection


@section('page-scripts')
    @include('app.dashboard.partials.scripts')
    <script src="{{ asset('assets/vendor/jquery-placeholder/jquery.placeholder.min.js') }}" charset="utf-8"></script>
    <script type="text/javascript">
        $(document).ready(function() {
            var s = $("#txtSchoolName");
            var g = $("#txtSchoolAddress");
            var x = $("#txtSchoolPhone");
            var p = $("#txtAdminName");
            var v = $("#txtAdminEmail");
            var m = $("#slctSchoolType");

            if(s.val() != "") {
                s.removeClass("empty");
            }

            if(g.val() != "") {
                g.removeClass("empty");
            }

            if(x.val() != "") {
                x.removeClass("empty");
            }

            if(p.val() != "") {
                p.removeClass("empty");
            }

            if(v.val() != "") {
                v.removeClass("empty");
            }

            if(m.val() != "") {
                m.removeClass("empty");
            }
        });
        $('#txtRePassword').on('change', function () {
            var e = $('#txtAdminPassword').val();
            var a = $('#txtRePassword').val();

            if (e != a) {
                $('.repeatpass').addClass('has-error');
            }
            else {
                $('.repeatpass').removeClass('has-error');
            }
        });

        $("#setupForm").submit(function (e) {
            e.preventDefault();

            if ($('.repeatpass').hasClass('has-error')) {
                return;
            }

            this.submit();
        });
    </script>
@endsection
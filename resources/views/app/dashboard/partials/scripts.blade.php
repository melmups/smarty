<script src="{{ asset('assets/vendor/chartist-js/chartist.min.js') }}"></script>
<script src="{{ asset('assets/vendor/chartist-plugin-tooltip/chartist-plugin-tooltip.min.js') }}"></script>
<script src="{{ asset('assets/vendor/jvectormap/jquery-jvectormap.min.js') }}"></script>
<script src="{{ asset('assets/vendor/jvectormap/maps/jquery-jvectormap-world-mill-en.js') }}"></script>
<script src="{{ asset('assets/vendor/matchheight/jquery.matchHeight-min.js') }}"></script>
<script src="{{ asset('assets/vendor/peity/jquery.peity.min.js') }}"></script>
<script src="{{ asset('assets/vendor/formvalidation/formValidation.min.js') }}"></script>
<script src="{{ asset('assets/vendor/formvalidation/framework/bootstrap.min.js') }}"></script>
<script src="{{ asset('assets/vendor/bootstrap-datepicker/bootstrap-datepicker.min.js') }}"></script>
<script src="{{ asset('assets/js/sweetalert.min.js') }}"></script>


<script src="{{ asset('assets/js/forms/validation.min.js') }}"></script>
<script src="{{ asset('assets/js/components/bootstrap-datepicker.min.js') }}"></script>
<script src="{{ asset('assets/js/dashboard/v1.min.js') }}"></script>
<script src="{{ asset('assets/js/dashboard.js') }}"></script>

<script type="text/javascript">
    $("#btnLogout").click(function () {
        window.location = "{{ url('/logout') }}";
    });
    $("#btnLock").click(function () {
        window.location = "{{ url('/app/lock') }}";
    });
    $("#btnSettings").click(function () {
        window.location = "{{ url('/app/admin/settings') }}";
    });
</script>

<script type="text/javascript">
    $(document).ready(function () {
        @include('app.partials.swal')

    });
</script>

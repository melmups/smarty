@extends('app.master')

@section('title') Setup your copy of Smarty | Smarty @endsection

@section('page-styles')
    @include('app.dashboard.partials.styles')
    <link rel="stylesheet" href="{{ asset('assets/css/pages/register.min3f0d.css') }}" media="screen">
    <link rel="stylesheet" href="{{ asset('assets/css/form-elements.css') }}" media="screen">
    <link rel="stylesheet" href="{{ asset('assets/css/style.css') }}" media="screen">
    <style media="screen">
        .text-white {
            text-align: justify;
            color: white;
        }
    </style>
    <script>
        window.setupInformation = {!! $classData !!} ;
        window.csrf = "{!! csrf_token() !!}";
    </script>
@endsection

@section('body-styles')page-register layout-full page-dark @endsection

@section('content')
    <div class="page animsition vertical-align text-center" data-animsition-in="fade-in"
         data-animsition-out="fade-out">
        <div class="page-content vertical-align-middle">

            <form role="form" action="" method="post" class="registration-form">

                <fieldset class="add-grade-form">
                    <div class="form-top">
                        <div class="form-top-left">
                            <h2>Step 1 / 3</h2>
                        </div>
                        <div class="form-top-right">
                            <i class="fa fa-user"></i>
                        </div>
                    </div>
                    <div class="form-bottom">
                        <div class="panel">
                            <div class="panel-body">
                                <div class="panel-heading">
                                    <h3 class="panel-title">Add Grade</h3>
                                </div>
                                    <div class="panel-body">
                                        <div class="add-data">
                                            <div class="form-group form-material">
                                                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                                <label class="col-lg-12 col-sm-3 control-label text-muted">Grade Level
                                                    <span class="required">*</span>
                                                </label>
                                                <input class="form-control" type="number" name="level" value="" required="" autofocus="">
                                            </div>

                                            <div class="form-group form-material">
                                                <label class="col-lg-12 col-sm-3 control-label text-muted">Label
                                                    <span class="required">*</span>
                                                </label>
                                                <input type="text" name="label" class="form-control" value="" required="">
                                            </div>


                                            <div class="form-group form-material">
                                                <button type="button" class="btn btn-add-grade btn-primary btn-block">Add grade</button>
                                            </div>
                                        </div>
                                    </div>
                            </div>
                            <div class="panel-footer">
                                <hr>
                                <button type="button" class="btn btn-success btn-next btn-block">Next Step</button>
                            </div>
                        </div>
                    </div>
                </fieldset>

                <fieldset class="add-class-form">
                    <div class="form-top">
                        <div class="form-top-left">
                            <h3>Step 2 / 3</h3>
                            <p>Add Classes</p>
                        </div>
                        <div class="form-top-right">
                            <i class="fa fa-key"></i>
                        </div>
                    </div>
                    <div class="form-bottom">
                        <div class="form-group">
                            <label class="sr-only" for="form-email">Email</label>
                            <input type="text" name="form-email" placeholder="Email..." class="form-email form-control" id="form-email">
                        </div>
                        <div class="form-group">
                            <label class="sr-only" for="form-password">Password</label>
                            <input type="password" name="form-password" placeholder="Password..." class="form-password form-control" id="form-password">
                        </div>
                        <div class="form-group">
                            <label class="sr-only" for="form-repeat-password">Repeat password</label>
                            <input type="password" name="form-repeat-password" placeholder="Repeat password..."
                                   class="form-repeat-password form-control" id="form-repeat-password">
                        </div>
                        <button type="button" class="btn btn-previous">Previous</button>
                        <button type="button" class="btn btn-next">Next</button>
                    </div>
                </fieldset>

                <fieldset>
                    <div class="form-top">
                        <div class="form-top-left">
                            <h3>Step 3 / 3</h3>
                            <p>Social media profiles:</p>
                        </div>
                        <div class="form-top-right">
                            <i class="fa fa-twitter"></i>
                        </div>
                    </div>
                    <div class="form-bottom">
                        <div class="form-group">
                            <label class="sr-only" for="form-facebook">Facebook</label>
                            <input type="text" name="form-facebook" placeholder="Facebook..." class="form-facebook form-control" id="form-facebook">
                        </div>
                        <div class="form-group">
                            <label class="sr-only" for="form-twitter">Twitter</label>
                            <input type="text" name="form-twitter" placeholder="Twitter..." class="form-twitter form-control" id="form-twitter">
                        </div>
                        <div class="form-group">
                            <label class="sr-only" for="form-google-plus">Google plus</label>
                            <input type="text" name="form-google-plus" placeholder="Google plus..." class="form-google-plus form-control" id="form-google-plus">
                        </div>
                        <button type="button" class="btn btn-previous">Previous</button>
                        <button type="submit" class="btn">Sign me up!</button>
                    </div>
                </fieldset>

            </form>

        </div>
    </div>
@endsection


@section('page-scripts')
    @include('app.dashboard.partials.scripts')
    <script src="{{ asset('assets/vendor/jquery-placeholder/jquery.placeholder.min.js') }}" charset="utf-8"></script>
    <script src="{{ asset('assets/js/jquery.backstretch.min.js') }}"></script>
    <script src="{{ asset('assets/js/retina-1.1.0.min.js') }}"></script>
    <script src="{{ asset('assets/js/scripts.js') }}"></script>

@endsection
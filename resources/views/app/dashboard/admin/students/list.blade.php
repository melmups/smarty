@extends('app.master')

@section('title')
    Students Listing | Smarty Dashboard
@endsection

@section('page-styles')
    @include('app.dashboard.partials.styles')
    <style media="screen">
        .table-controls {
            margin-bottom: 20px;
        }

        .table-controls form {
            margin-right: 25px;
        }

        .table-controls form .form-control {
            width: 100px;
            margin-right: 8px;
        }

        .table-controls form input[type="text"] {
            width: 200px;
        }

        @media screen and (max-width: 780px) {
            .table-controls {
                display: none;
            }
        }
    </style>
@endsection

@section('body-styles')
    dashboard
@endsection

@section('content')
    <nav class="@include('app.dashboard.partials.nav-styles')" role="navigation">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle hamburger hamburger-close navbar-toggle-left hided"
                    data-toggle="menubar">
                <span class="sr-only">Toggle navigation</span>
                <span class="hamburger-bar"></span>
            </button>
            <button type="button" class="navbar-toggle collapsed" data-target="#site-navbar-collapse"
                    data-toggle="collapse">
                <i class="icon md-more" aria-hidden="true"></i>
            </button>
            <div class="navbar-brand navbar-brand-center site-gridmenu-toggle" data-toggle="gridmenu">
                <img class="navbar-brand-logo" src="{{ asset('assets/images/logo.png') }}" title="Smarty">
                <span class="navbar-brand-text hidden-xs"> Smarty</span>
            </div>
            <button type="button" class="navbar-toggle collapsed" data-target="#site-navbar-search"
                    data-toggle="collapse">
                <span class="sr-only">Toggle Search</span>
                <i class="icon md-search" aria-hidden="true"></i>
            </button>
        </div>

        <div class="navbar-container container-fluid">
            <!-- Navbar Collapse -->
            <div class="collapse navbar-collapse navbar-collapse-toolbar" id="site-navbar-collapse">
                <!-- Navbar Toolbar -->
                <ul class="nav navbar-toolbar">
                    <li class="hidden-float" id="toggleMenubar">
                        <a data-toggle="menubar" href="#" role="button">
                            <i class="icon hamburger hamburger-arrow-left">
                                <span class="sr-only">Toggle menubar</span>
                                <span class="hamburger-bar"></span>
                            </i>
                        </a>
                    </li>
                    <li class="hidden-xs" id="toggleFullscreen">
                        <a class="icon icon-fullscreen" data-toggle="fullscreen" href="#" role="button">
                            <span class="sr-only">Toggle fullscreen</span>
                        </a>
                    </li>
                    <li class="hidden-float">
                        <a class="icon md-search" data-toggle="collapse" href="#" data-target="#site-navbar-search"
                           role="button">
                            <span class="sr-only">Toggle Search</span>
                        </a>
                    </li>
                </ul>
                <!-- End Navbar Toolbar -->

                <!-- Navbar Toolbar Right -->
                <ul class="nav navbar-toolbar navbar-right navbar-toolbar-right">
                    <li class="dropdown">
                        <a class="navbar-avatar dropdown-toggle" data-toggle="dropdown" href="#" aria-expanded="false"
                           data-animation="scale-up" role="button">
              <span class="avatar avatar-online">
                <img src="{{ asset('assets/portraits/5.jpg') }}" alt="...">
                <i></i>
              </span>
                        </a>
                        <ul class="dropdown-menu" role="menu">
                            <li role="presentation">
                                <a href="javascript:void(0)" role="menuitem"><i class="icon md-account"
                                                                                aria-hidden="true"></i> Profile</a>
                            </li>
                            <li role="presentation">
                                <a href="javascript:void(0)" role="menuitem"><i class="icon md-card"
                                                                                aria-hidden="true"></i> Billing</a>
                            </li>
                            <li role="presentation">
                                <a href="javascript:void(0)" role="menuitem"><i class="icon md-settings"
                                                                                aria-hidden="true"></i> Settings</a>
                            </li>
                            <li class="divider" role="presentation"></li>
                            <li role="presentation">
                                <a href="javascript:void(0)" id="btnLogout" role="menuitem"><i class="icon md-power"
                                                                                               aria-hidden="true"></i>
                                    Logout</a>
                            </li>
                        </ul>
                    </li>
                    <li class="dropdown">
                        <a data-toggle="dropdown" href="javascript:void(0)" title="Notifications" aria-expanded="false"
                           data-animation="scale-up" role="button">
                            <i class="icon md-notifications" aria-hidden="true"></i>
                            <span class="badge badge-danger up">5</span>
                        </a>
                        <ul class="dropdown-menu dropdown-menu-right dropdown-menu-media" role="menu">
                            <li class="dropdown-menu-header" role="presentation">
                                <h5>NOTIFICATIONS</h5>
                                <span class="label label-round label-danger">New 5</span>
                            </li>

                            <li class="list-group" role="presentation">
                                <div data-role="container">
                                    <div data-role="content">
                                        <a class="list-group-item" href="javascript:void(0)" role="menuitem">
                                            <div class="media">
                                                <div class="media-left padding-right-10">
                                                    <i class="icon md-receipt bg-red-600 white icon-circle"
                                                       aria-hidden="true"></i>
                                                </div>
                                                <div class="media-body">
                                                    <h6 class="media-heading">A new order has been placed</h6>
                                                    <time class="media-meta" datetime="2015-06-12T20:50:48+08:00">5
                                                        hours ago
                                                    </time>
                                                </div>
                                            </div>
                                        </a>
                                        <a class="list-group-item" href="javascript:void(0)" role="menuitem">
                                            <div class="media">
                                                <div class="media-left padding-right-10">
                                                    <i class="icon md-account bg-green-600 white icon-circle"
                                                       aria-hidden="true"></i>
                                                </div>
                                                <div class="media-body">
                                                    <h6 class="media-heading">Completed the task</h6>
                                                    <time class="media-meta" datetime="2015-06-11T18:29:20+08:00">2 days
                                                        ago
                                                    </time>
                                                </div>
                                            </div>
                                        </a>
                                        <a class="list-group-item" href="javascript:void(0)" role="menuitem">
                                            <div class="media">
                                                <div class="media-left padding-right-10">
                                                    <i class="icon md-settings bg-red-600 white icon-circle"
                                                       aria-hidden="true"></i>
                                                </div>
                                                <div class="media-body">
                                                    <h6 class="media-heading">Settings updated</h6>
                                                    <time class="media-meta" datetime="2015-06-11T14:05:00+08:00">2 days
                                                        ago
                                                    </time>
                                                </div>
                                            </div>
                                        </a>
                                        <a class="list-group-item" href="javascript:void(0)" role="menuitem">
                                            <div class="media">
                                                <div class="media-left padding-right-10">
                                                    <i class="icon md-calendar bg-blue-600 white icon-circle"
                                                       aria-hidden="true"></i>
                                                </div>
                                                <div class="media-body">
                                                    <h6 class="media-heading">Event started</h6>
                                                    <time class="media-meta" datetime="2015-06-10T13:50:18+08:00">3 days
                                                        ago
                                                    </time>
                                                </div>
                                            </div>
                                        </a>
                                        <a class="list-group-item" href="javascript:void(0)" role="menuitem">
                                            <div class="media">
                                                <div class="media-left padding-right-10">
                                                    <i class="icon md-comment bg-orange-600 white icon-circle"
                                                       aria-hidden="true"></i>
                                                </div>
                                                <div class="media-body">
                                                    <h6 class="media-heading">Message received</h6>
                                                    <time class="media-meta" datetime="2015-06-10T12:34:48+08:00">3 days
                                                        ago
                                                    </time>
                                                </div>
                                            </div>
                                        </a>
                                    </div>
                                </div>
                            </li>
                            <li class="dropdown-menu-footer" role="presentation">
                                <a class="dropdown-menu-footer-btn" href="javascript:void(0)" role="button">
                                    <i class="icon md-settings" aria-hidden="true"></i>
                                </a>
                                <a href="javascript:void(0)" role="menuitem">
                                    All notifications
                                </a>
                            </li>
                        </ul>
                    </li>
                </ul>
                <!-- End Navbar Toolbar Right -->
            </div>
            <!-- End Navbar Collapse -->

            <!-- Site Navbar Seach -->
            <div class="collapse navbar-search-overlap" id="site-navbar-search">
                <form role="search">
                    <div class="form-group">
                        <div class="input-search">
                            <i class="input-search-icon md-search" aria-hidden="true"></i>
                            <input type="text" class="form-control" name="site-search" placeholder="Search...">
                            <button type="button" class="input-search-close icon md-close"
                                    data-target="#site-navbar-search"
                                    data-toggle="collapse" aria-label="Close"></button>
                        </div>
                    </div>
                </form>
            </div>
            <!-- End Site Navbar Seach -->
        </div>
    </nav>
    <div class="@include('app.dashboard.partials.menubar')">
        <div class="site-menubar-body">
            <div>
                <div>
                    <ul class="site-menu">
                        <li class="site-menu-category">General</li>
                        <li class="site-menu-item">
                            <a class="animsition-link" href="{{ url('/app/admin') }}">
                                <i class="site-menu-icon md-view-dashboard" aria-hidden="true"></i>
                                <span class="site-menu-title">Dashboard</span>
                            </a>
                        </li>
                        <li class="site-menu-category">Teachers</li>
                        <li class="site-menu-item has-sub">
                            <a href="javascript:void(0)">
                                <i class="site-menu-icon md-case" aria-hidden="true"></i>
                                <span class="site-menu-title">Manage teachers</span>
                                <span class="site-menu-arrow"></span>
                            </a>
                            <ul class="site-menu-sub">
                                <li class="site-menu-item">
                                    <a class="animsition-link" href="{{ url('/app/admin/staff/add') }}">
                                        <span class="site-menu-title">Add teachers</span>
                                    </a>
                                </li>
                                <li class="site-menu-item active">
                                    <a class="animsition-link" href="{{ url('/app/admin/staff/list') }}">
                                        <span class="site-menu-title">View teachers</span>
                                    </a>
                                </li>
                            </ul>
                        </li>
                        <li class="site-menu-item">
                            <a class="animsition-link" href="{{ url('/app/admin/staff/clock-ins') }}">
                                <i class="site-menu-icon md-chart" aria-hidden="true"></i>
                                <span class="site-menu-title">Clock-in records</span>
                            </a>
                        </li>

                        <li class="site-menu-category">Students</li>
                        <li class="site-menu-item has-sub active open">
                            <a href="javascript:void(0)">
                                <i class="site-menu-icon md-graduation-cap" aria-hidden="true"></i>
                                <span class="site-menu-title">Manage students</span>
                                <span class="site-menu-arrow"></span>
                            </a>
                            <ul class="site-menu-sub">
                                <li class="site-menu-item">
                                    <a class="animsition-link" href="{{ url('/app/admin/students/add') }}">
                                        <span class="site-menu-title">Register Student</span>
                                    </a>
                                </li>
                                <li class="site-menu-item active">
                                    <a class="animsition-link" href="{{ url('/app/admin/students/list') }}">
                                        <span class="site-menu-title">View students</span>
                                    </a>
                                </li>
                            </ul>
                        </li>
                        <li class="site-menu-item">
                            <a class="animsition-link" href="{{ url('/app/admin/students/attendance') }}">
                                <i class="site-menu-icon md-chart" aria-hidden="true"></i>
                                <span class="site-menu-title">Student attendance</span>
                            </a>
                        </li>

                        <li class="site-menu-category">Other</li>
                        <li class="site-menu-item has-sub">
                            <a href="javascript:void(0)">
                                <i class="site-menu-icon md-money-box" aria-hidden="true"></i>
                                <span class="site-menu-title">Fees</span>
                                <span class="site-menu-arrow"></span>
                            </a>
                            <ul class="site-menu-sub">
                                <li class="site-menu-item">
                                    <a class="animsition-link" href="{{ url('/app/admin/fees/add') }}">
                                        <span class="site-menu-title">Add Payment</span>
                                    </a>
                                </li>
                                <li class="site-menu-item">
                                    <a class="animsition-link" href="{{ url('/app/admin/fees/list') }}">
                                        <span class="site-menu-title">View Payments</span>
                                    </a>
                                </li>
                            </ul>
                        </li>

                        <li class="site-menu-item has-sub">
                            <a href="javascript:void(0)">
                                <i class="site-menu-icon md-assignment" aria-hidden="true"></i>
                                <span class="site-menu-title">Exams</span>
                                <span class="site-menu-arrow"></span>
                            </a>
                            <ul class="site-menu-sub">
                                <li class="site-menu-item">
                                    <a class="animsition-link" href="{{ url('/app/admin/exams/add') }}">
                                        <span class="site-menu-title">Add exam</span>
                                    </a>
                                </li>
                                <li class="site-menu-item">
                                    <a class="animsition-link" href="{{ url('/app/admin/results/add') }}">
                                        <span class="site-menu-title">Record Results</span>
                                    </a>
                                </li>
                                <li class="site-menu-item">
                                    <a class="animsition-link" href="{{ url('/app/admin/results/analytics') }}">
                                        <span class="site-menu-title">View charts</span>
                                    </a>
                                </li>
                            </ul>
                        </li>

                        <li class="site-menu-item has-sub">
                            <a href="javascript:void(0)">
                                <i class="site-menu-icon md-collection-text" aria-hidden="true"></i>
                                <span class="site-menu-title">Classes</span>
                                <span class="site-menu-arrow"></span>
                            </a>
                            <ul class="site-menu-sub">
                                <li class="site-menu-item">
                                    <a class="animsition-link" href="{{ url('/app/admin/class/add') }}">
                                        <span class="site-menu-title">Add class</span>
                                    </a>
                                </li>
                                <li class="site-menu-item">
                                    <a class="animsition-link" href="{{ url('/app/admin/class/list') }}">
                                        <span class="site-menu-title">View classes</span>
                                    </a>
                                </li>
                                <li class="site-menu-item">
                                    <a class="animsition-link" href="{{ url('/app/admin/class/add-grade') }}">
                                        <span class="site-menu-title">Grade Levels</span>
                                    </a>
                                </li>
                            </ul>
                        </li>
                        <li class="site-menu-item">
                            <a class="animsition-link" href="{{ url('/app/admin/settings') }}">
                                <i class="site-menu-icon md-settings" aria-hidden="true"></i>
                                <span class="site-menu-title">Settings</span>
                            </a>
                        </li>

                    </ul>
                </div>
            </div>
        </div>

        <div class="site-menubar-footer">
            <a href="javascript: void(0);" id="btnSettings" class="fold-show" data-placement="top" data-toggle="tooltip"
               data-original-title="Settings">
                <span class="icon md-settings" aria-hidden="true"></span>
            </a>
            <a href="javascript: void(0);" id="btnLock" data-placement="top" data-toggle="tooltip"
               data-original-title="Lock">
                <span class="icon md-eye-off" aria-hidden="true"></span>
            </a>
            <a href="javascript: void(0);" id="btnLogout" data-placement="top" data-toggle="tooltip"
               data-original-title="Logout">
                <span class="icon md-power" aria-hidden="true"></span>
            </a>
        </div>
    </div>
    <div class="site-gridmenu">
        <div>
            <div>
                <ul>

                    <li>
                        <a href="{{ url('/app/admin/fees/add') }}">
                            <i class="icon md-money-box"></i>
                            <span>Fees Payment</span>
                        </a>
                    </li>
                    <li>
                        <a href="{{ url('/app/admin/settings') }}">
                            <i class="icon md-settings"></i>
                            <span>Settings</span>
                        </a>
                    </li>
                    <li>
                        <a href="{{ url('/app/admin/students/add') }}">
                            <i class="icon md-graduation-cap"></i>
                            <span>Register Student</span>
                        </a>
                    </li>
                    <li>
                        <a href="{{ url('/admin/staff/add') }}">
                            <i class="icon md-case"></i>
                            <span>Add Teacher</span>
                        </a>
                    </li>

                </ul>
            </div>
        </div>
    </div>


    <!-- Page -->
    <div class="page animsition">
        <div class="page-content container-fluid">
            <!-- Listings Table -->
            <div class="panel pnlStudentListings">
                <header class="panel-heading">
                    <h3 class="panel-title">Students Listing</h3>

                    <div class="table-controls">
                        <div class="row">
                            <div class="col-md-5">

                            </div>
                            <div class="col-md-7">
                                <form class="form-inline pull-right" action="{{ url("/app/admin/students/list") }}"
                                      aria-hidden="true" role="search" method="get">
                                    <div class="form-group form-material">
                                        <select class="form-control" name="perPage">
                                            <option value="">10</option>
                                            <option value="25" <?php if (isset($_GET['perPage'])) {
                                                $cl = (int)$_GET['perPage'];
                                                if ($cl == 25) echo 'selected=""';
                                            } ?>>25
                                            </option>
                                            <option value="50"<?php if (isset($_GET['perPage'])) {
                                                $cl = (int)$_GET['perPage'];
                                                if ($cl == 50) echo 'selected=""';
                                            } ?>>50
                                            </option>
                                        </select>
                                    </div>
                                    <div class="form-group form-material">
                                        <select class="form-control" name="class" required>
                                            <option value="0" <?php if (!isset($_GET['class'])) echo 'selected=""'; ?>>
                                                All
                                            </option>
                                            @foreach($classes as $class)
                                                <option value="{{ $class->id }}" <?php if (isset($_GET['class'])) {
                                                    $cl = (int)$_GET['class'];
                                                    if ($cl == $class->id) echo 'selected=""';
                                                } ?>>{{ $class->name }}</option>
                                            @endforeach
                                        </select>
                                    </div>

                                    <div class="form-group form-material">
                                        <input type="text" class="form-control" name="s" placeholder="Search..."
                                               value="<?php if (isset($_GET['s'])) echo e($_GET['s']); ?>"/>
                                    </div>

                                    <div class="form-group form-material">
                                        <button type="submit" class="btn btn-default">Filter</button>
                                    </div>

                                    @if(isset($_GET['s']) || isset($_GET['class']))
                                        <div class="form-group form-material">
                                            <a href="javascript:void()"
                                               onclick="javascript:window.location='{{ url('app/admin/students/list') }}'"
                                               class="btn btn-default">Reset</a>
                                        </div>
                                    @endif
                                </form>
                            </div>
                        </div>
                    </div>
                </header>

                <div class="panel-body">
                    <table class="table table-hover table-striped width-full">
                        <thead>
                        <tr>
                            <th>Name</th>
                            <th>Sex</th>
                            <th>Date of birth</th>
                            <th>Birth Entry Number</th>
                            <th>Class</th>
                            <th>Actions</th>
                        </tr>
                        </thead>
                        <tfoot>
                        <tr>
                            <th>Name</th>
                            <th>Sex</th>
                            <th>Date of birth</th>
                            <th>Birth Entry Number</th>
                            <th>Class</th>
                            <th>Actions</th>
                        </tr>
                        </tfoot>
                        <tbody>
                        @if($students->count())

                            @foreach($students as $student)
                                <tr>
                                    <td>{{ sprintf("%s %s", $student->firstNames, $student->lastName) }}</td>
                                    <td>{{ ucfirst($student->gender) }}</td>
                                    <td>{{ with(new Carbon\Carbon($student->dateOfBirth))->toFormattedDateString() }}</td>
                                    <td>{{ $student->birthEntryNo }}</td>
                                    <td>{{ with(new Smarty\Helper)->getClassNameById($student->classId) }}</td>
                                    <td></td>
                                </tr>
                            @endforeach

                        @endif
                        </tbody>
                    </table>
                </div>

                <div class="panel-footer">
                    @include('app.dashboard.partials.pagination', ['paginator' => $students])
                </div>
            </div>
            <!-- End Listing Table -->
        </div>
    </div>
    <!-- End Page -->


    @include('app.dashboard.partials.footer')

@endsection

@section('page-scripts')
    @include('app.dashboard.partials.scripts')
    <script src="{{ asset('assets/vendor/datatables/jquery.dataTables.min.js') }}" charset="utf-8"></script>
    <script src="{{ asset('assets/vendor/datatables-bootstrap/dataTables.bootstrap.min.js') }}"
            charset="utf-8"></script>
    <script src="{{ asset('assets/vendor/datatables-tabletools/dataTables.tableTools.js') }}" charset="utf-8"></script>

    <script src="{{ asset('assets/js/components/datatables.min.js') }}" charset="utf-8"></script>
    <script src="{{ asset('assets/js/tables/datatable.min.js') }}" charset="utf-8"></script>

@endsection

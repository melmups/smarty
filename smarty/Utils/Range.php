<?php
namespace Smarty\Utils;

class Range
{
    private $startDate;

    private $endDate;


    public function __construct($start, $end)
    {
        $this->startDate = $start;
        $this->endDate = $end;
    }

    public function getStartDate()
    {
        return $this->startDate;
    }


    public function getEndDate()
    {
        return $this->endDate;
    }
}

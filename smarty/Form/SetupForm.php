<?php
namespace Smarty\Form;

class SetupForm extends Form
{
    protected $rules = [
        'schoolName' => 'required',
        'schoolAddress' => 'required',
        'schoolPhone' => 'required',
        'schoolType' => "required|in:primary,secondary",
        'adminName' => 'required|min:6',
        'adminEmail' => 'required|email',
        'adminPassword' => 'required|min:8',
        'adminRePassword' => 'required|same:adminPassword',
        'schoolLogo' => "image|required"
    ];
}

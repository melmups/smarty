<?php
namespace Smarty\Form;

class AddClassForm extends Form
{
    protected $rules = [
        'identifier' => 'required',
        'gradeId' => 'required',
    ];
}

<?php
namespace Smarty\Form;

class AddTeacherForm extends Form
{
    protected $rules = [
        'email' => 'required|email',
        'password' => 'required|min:6',
        'firstNames' => "required",
        'lastName' => "required",
        'gender' => "required",
        'dateOfBirth' => "required",
        'nationalId' => "required",
        'ecNumber' => "required",
        'phone' => "required",
        'homeAddress' => "required",
        "nextOfKin" => "required",
        'type' => "required"
    ];
}

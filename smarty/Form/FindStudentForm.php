<?php
namespace Smarty\Form;

class FindStudentForm extends Form
{
    protected $rules = [
        'lastName' => 'required',
        'class' => 'required',
    ];
}

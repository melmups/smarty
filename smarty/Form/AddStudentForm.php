<?php
namespace Smarty\Form;

class AddStudentForm extends Form
{
    protected $rules = [
        'firstNames' => "required",
        'lastName' => "required",
        'gender' => "required",
        'dateOfBirth' => "required",
        'birthEntryNo' => "required",
        'residentialArea' => "required",
        'guardianName' => "required",
        'guardianPhone' => "required",
        'medicalConditions' => "required",
        "classId" => "required"
    ];
}

<?php
namespace Smarty\Form;

class AddGradeForm extends Form
{
    protected $rules = [
        'level' => 'required',
        'label' => 'required',
    ];
}

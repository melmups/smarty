<?php
namespace Smarty\Form;

use Illuminate\Support\MessageBag;

/**
 * This is the exception thrown when our user's input fails the validation tests
 *
 * @package Alcra\Form
 */
class ValidationException extends \Exception
{
    protected $errors;

    /**
     * ValidationException constructor.
     * @param MessageBag $errors
     */
    public function __construct(MessageBag $errors)
    {
        $this->errors = $errors;
    }

    /**
     * Returns an array containing all of our application's errors
     * @return array
     */
    public function getErrors()
    {
        $err = [];

        foreach ($this->errors->all() as $error) {
            $err[] = [
                "title" => "Something is wrong with your input",
                "type" => "danger",
                "message" => $error
            ];
        }
        return $err;
    }
}

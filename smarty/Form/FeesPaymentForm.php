<?php
namespace Smarty\Form;

class FeesPaymentForm extends Form
{
    protected $rules = [
        'studentId' => 'required',
        'amount' => 'required',
        'paymentDate' => 'required',
        'recieptId' => 'required'
    ];
}

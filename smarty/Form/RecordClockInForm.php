<?php
namespace Smarty\Form;


class RecordClockInForm extends Form
{
    protected $rules = [
        'teacherEcNumber' => 'required|min:4',
    ];
}
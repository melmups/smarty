<?php
namespace Smarty\Form;

use Illuminate\Foundation\Validation\ValidatesRequests;
use Validator;


abstract class Form
{
    use ValidatesRequests;

    public function validate($data)
    {
        if (!isset($this->rules)) {
            throw new \Exception("No rules were specified");
        }

        try {
            $validator = Validator::make($data, $this->rules);
        } catch (\Exception $e) {
            abort(500);
        }

        if ($validator->fails()) {
            throw new ValidationException($validator->messages());
        }
        return true;
    }
}

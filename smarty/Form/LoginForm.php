<?php
namespace Smarty\Form;


class LoginForm extends Form
{
    protected $rules = [
        'email' => 'required|email',
        'password' => 'required|min:6'
    ];
}

<?php
namespace Smarty\School;

use Carbon\Carbon;

class Term
{
    /**
     * Represents the start and end of the first term
     * @var string
     */
    protected $termOne = "01-01::31-04";

    /**
     * Represents the start and end of the second term
     * @var string
     */
    protected $termTwo = "01-05::31-08";

    /**
     * Represents the start and end of the third term
     * @var string
     */
    protected $termThree = "01-09::31-12";

    /**
     * Term constructor.
     */
    public function __construct()
    {

    }

    /**
     * Gets the current term at current date and time
     */
    public function getCurrentTerm()
    {
        $month = (int)date('m');

        if ($month >= $this->getFirstTermStartDate()->month && $month <= $this->getFirstTermEndDate()->month) {
            return 1;
        } elseif ($month >= $this->getSecondTermStartDate()->month && $month <= $this->getSecondTermEndDate()->month) {
            return 2;
        } elseif ($month >= $this->getThirdTermStartDate()->month && $month <= $this->getThirdTermEndDate()->month) {
            return 3;
        }

        return null;
    }

    /**
     * Get the start date of the first term
     * @param $year int The year you want to get first term start date for
     * @return Carbon An instance of Carbon representing the date
     */
    public function getFirstTermStartDate($year = null)
    {
        $startDate = explode("::", $this->termOne)[0];

        if (is_null($year)) {
            return new Carbon($startDate . "-" . date("Y"));
        } else {
            return new Carbon($startDate . "-" . $year);
        }
    }

    /**
     * Get the start date of the first term
     * @param $year int The year you want to get first term start date for
     * @return Carbon An instance of Carbon representing the date
     */
    public function getFirstTermEndDate($year = null)
    {
        $endDate = explode("::", $this->termOne)[1];

        if (is_null($year)) {
            return new Carbon($endDate . "-" . date("Y"));
        } else {
            return new Carbon($endDate . "-" . $year);
        }
    }

    /**
     * Get the start date of the second term
     * @param $year int The year you want to get first term start date for
     * @return Carbon An instance of Carbon representing the date
     */
    public function getSecondTermStartDate($year = null)
    {
        $startDate = explode("::", $this->termTwo)[0];

        if (is_null($year)) {
            return new Carbon($startDate . "-" . date("Y"));
        } else {
            return new Carbon($startDate . "-" . $year);
        }
    }

    /**
     * Get the start date of the second term
     * @param $year int The year you want to get first term start date for
     * @return Carbon An instance of Carbon representing the date
     */
    public function getSecondTermEndDate($year = null)
    {
        $endDate = explode("::", $this->termTwo)[1];

        if (is_null($year)) {
            return new Carbon($endDate . "-" . date("Y"));
        } else {
            return new Carbon($endDate . "-" . $year);
        }
    }

    /**
     * Get the start date of the second term
     * @param $year int The year you want to get first term start date for
     * @return Carbon An instance of Carbon representing the date
     */
    public function getThirdTermStartDate($year = null)
    {
        $startDate = explode("::", $this->termThree)[0];

        if (is_null($year)) {
            return new Carbon($startDate . "-" . date("Y"));
        } else {
            return new Carbon($startDate . "-" . $year);
        }
    }

    /**
     * Get the start date of the second term
     * @param $year int The year you want to get first term start date for
     * @return Carbon An instance of Carbon representing the date
     */
    public function getThirdTermEndDate($year = null)
    {
        $endDate = explode("::", $this->termThree)[1];

        if (is_null($year)) {
            return new Carbon($endDate . "-" . date("Y"));
        } else {
            return new Carbon($endDate . "-" . $year);
        }
    }

    /**
     * Get the first date of the first term
     * @return string The first date of the term
     */
    public function getFirstTermStartDateString()
    {
        $startDate = explode("::", $this->termOne)[0];

        return $startDate . "-" . date("Y");
    }

    /**
     * Get the first date of the first term
     * @return string The first date of the term
     */
    public function getFirstTermEndDateString()
    {
        $endDate = explode("::", $this->termOne)[1];

        return $endDate . "-" . date("Y");
    }

    /**
     * Get the first date of the second term
     * @return string The first date of the term
     */
    public function getSecondTermStartDateString()
    {
        $startDate = explode("::", $this->termTwo)[0];

        return $startDate . "-" . date("Y");
    }

    /**
     * Get the first date of the first term
     * @return string The first date of the term
     */
    public function getSecondTermEndDateString()
    {
        $endDate = explode("::", $this->termTwo)[1];

        return $endDate . "-" . date("Y");
    }

    /**
     * Get the first date of the second term
     * @return string The first date of the term
     */
    public function getThirdTermStartDateString()
    {
        $startDate = explode("::", $this->termThree)[0];

        return $startDate . "-" . date("Y");
    }

    /**
     * Get the first date of the first term
     * @return string The first date of the term
     */
    public function getThirdTermEndDateString()
    {
        $endDate = explode("::", $this->termThree)[1];

        return $endDate . "-" . date("Y");
    }

}

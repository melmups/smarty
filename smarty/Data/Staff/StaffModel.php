<?php
namespace Smarty\Data\Staff;

use App\Models\Staff;
use App\Models\User;

/**
 * This class contains logic necessary to interact with the Staff model
 */
class StaffModel
{

    /**
     * Add a staff member to the application
     * @param array $data
     * @return mixed
     */
    public static function add(array $data)
    {
        $user = new User([
            "name" => sprintf("%s %s", $data['firstNames'], $data['lastName']),
            "email" => $data['email'],
            "password" => \Hash::make($data['password']),
            "type" => 1
        ]);

        if ($user->save()) {
            $id = $user->id;

            $staff = new Staff();

            $staff->firstNames = $data['firstNames'];
            $staff->lastName = $data["lastName"];
            $staff->gender = $data["gender"];
            $staff->dateOfBirth = $data["dateOfBirth"];
            $staff->nationalId = $data['nationalId'];
            $staff->ecNumber = $data['ecNumber'];
            $staff->phone = $data['phone'];
            $staff->nextOfKin = $data['nextOfKin'];
            $staff->homeAddress = $data['homeAddress'];
            $staff->type = $data["type"];
            $staff->staffId = $id;

            if ($staff->save()) {
                return true;
            }
        }
        return false;
    }
}

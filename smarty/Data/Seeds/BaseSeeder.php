<?php
namespace Smarty\Data\Seeds;

use DB;
use Illuminate\Database\Seeder;

abstract class BaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        if (!isset($this->table)) {
            throw new SeederException("No table was specified");
        }

        if (method_exists(get_class(), 'getData')) {
            throw new SeederException("No input data");
        }

        DB::table($this->table)->truncate();
        DB::table($this->table)->insert($this->getData());
    }
}

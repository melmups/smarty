<?php
namespace Smarty\Subscribers;

/**
 * Listens for an event when a new student is added the deletes the application
 * cache
 */
class StudentAdd
{
    public function handle()
    {
        $this->onStudentAdd();
    }

    public function onStudentAdd()
    {
        $dir = storage_path() . "/framework/views";

        $directory = $dir;

        if (!$dirhandle = @opendir($directory)) {
            return;
        }

        while (false !== ($filename = readdir($dirhandle))) {
            if ($filename != "." && $filename != "..") {
                $filename = $directory . "/" . $filename;
                @unlink($filename);
            }
        }
    }

    public function subscribe($events)
    {
        $events->listen('student.Add', "Alcra\Subscribers\StudentAdd@onStudentAdd");
    }
}

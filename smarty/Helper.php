<?php
namespace Smarty;

use App\Models\Classroom;
use App\Models\Grade;
use App\Models\Student;

use Setting;

/**
 * Helper class for Smarty SAS
 */
class Helper
{

    public function insertPrimaryGrades() {
        for ($i = 0; $i < 8; $i++) {
            $grade = new Grade();

            $grade->level = $i;
            $grade->label = "Grade " . $i;

            $grade->save();
        }
    }

    public function insertSecondaryGrades() {
        for ($i = 1; $i < 7; $i++) {
            $grade = new Grade();

            $grade->level = $i;
            $grade->label = "Form " . $i;

            $grade->save();
        }
    }

    public function getClasses()
    {
        $classes = [];

        foreach (Classroom::all() as $class) {
            $classes[] = (object)[
                'id' => $class->id,
                'name' => $this->getClassName($class->gradeId, $class->identifier),
                'students' => $this->countClassStudents($class->id)
            ];
        }

        return $classes;
    }

    /**
     * Gets the full name of a classroom
     * @param  int $gradeId The grade's id
     * @param  string $identifier Class's unique identifier
     * @return string          Class name
     */
    public function getClassName(int $gradeId, $identifier)
    {
        $label = Grade::where('id', '=', $gradeId)->first()->label;

        return $label . $identifier;
    }

    public function countClassStudents($classId)
    {
        return Student::where("classId", "=", $classId)->count();
    }

    public function getStudents()
    {
        $students = [];
        // TODO: Update function to use SQL join instead of loop.
        // REVIEW: Loop impractical if records are in the thousands

        foreach (Student::all() as $student) {

            $students[] = (object)[
                "name" => sprintf("%s %s", $student->firstNames, $student->lastName),
                //TODO: Use Carbon to print out good looking date
                "dateOfBirth" => $student->dateOfBirth,
                "gender" => $student->gender,
                "birthEntryNo" => $student->birthEntryNo,
                "residentialArea" => $student->residentialArea,
                "guardianName" => $student->guardianName,
                "class" => $this->getClassNameById($student->classId),
                "id" => $student->id
            ];
        }

        return $students;
    }

    /**
     * Gets the full name of a classroom
     * @param  int $id The class' id
     * @return string          Class name
     */
    public function getClassNameById($id)
    {
        $class = Classroom::where('id', '=', $id)->first();

        $label = Grade::where('id', '=', $class->gradeId)->first()->label;

        return $label . $class->identifier;
    }

    /**
     * Calculates the ratio of female to male students
     * @return array
     */
    public function calculateMaleToFemaleRatio()
    {
        $maleCount = Student::where("gender", "=", "male")->get()->count();
        $femaleCount = Student::where("gender", "=", "female")->get()->count();

        return [
            (object)[
                "value" => $maleCount,
                "color" => "blue",
                "label" => "Boys"
            ],
            (object)[
                "value" => $femaleCount,
                "color" => "red",
                "label" => "Girls"
            ],
        ];
    }

    /**
     * Return a bool value representing whether application has been setup or not
     * @return bool
     */
    public function isAppSetup()
    {
        return (bool)Setting::get('setup') === true;
    }

    /**
     * Return a bool value representing whether classes have been added or not
     * @return bool
     */
    public function isClassesAdded()
    {
        return (bool)Setting::get('classes_added') === true;
    }

    /**
     * Return a bool value representing whether grades have been added or not
     * @return bool
     */
    public function isGradesAdded()
    {
        return (bool)Setting::get('grades_added') === true;
    }
}
